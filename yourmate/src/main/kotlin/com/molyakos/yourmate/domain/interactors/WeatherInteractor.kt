package com.molyakos.yourmate.domain.interactors

import com.molyakos.yourmate.domain.models.WeatherModel
import com.molyakos.yourmate.domain.repository.IWeatherRepository
import com.molyakos.yourmate.utils.IRxSchedulers
import io.reactivex.Observable

interface IWeatherInteractor {
    fun updateWeather(cityName: String, units: String): Observable<WeatherModel>
}

class WeatherInteractor(private val weatherRepository: IWeatherRepository,
                        private val schedulers: IRxSchedulers) : IWeatherInteractor {

    override fun updateWeather(cityName: String, units: String): Observable<WeatherModel> {
        return Observable.concat(weatherRepository.fetchWeatherFromDiskStore(), weatherRepository.fetchWeatherFromCloudStore(cityName, units))
                .compose(schedulers.applySchedulers())
    }
}