package com.molyakos.yourmate.domain.controllers

interface IClockController {
    fun prepare(callback: (String) -> Unit)
    fun destroy()
}