package com.molyakos.yourmate.domain.interactors

import com.molyakos.yourmate.domain.repository.ISMSRepository
import com.molyakos.yourmate.utils.IRxSchedulers
import io.reactivex.Observable

interface ISMSInteractor {
    fun updateMissedSMSCount() : Observable<Int>
}

class SMSInteractor(private val smsRepository: ISMSRepository,
                    private val schedulers: IRxSchedulers): ISMSInteractor {

    override fun updateMissedSMSCount(): Observable<Int> {
        return smsRepository.fetchMissedSMSCount().compose(schedulers.applySchedulers())
    }
}