package com.molyakos.yourmate.domain.interactors

import com.molyakos.yourmate.domain.repository.ISettingsRepository

interface ISettingsInteractor {
    fun fetchCurrentCityName(): String
    fun fetchUnits(): String
}

class SettingsInteractor(private val settingsRepository: ISettingsRepository): ISettingsInteractor {
    override fun fetchCurrentCityName(): String {
        return settingsRepository.fetchCurrentCityName()
    }

    override fun fetchUnits(): String {
        return settingsRepository.fetchUnits()
    }
}