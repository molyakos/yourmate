package com.molyakos.yourmate.domain.controllers

interface IToneController {
    fun prepare()
    fun keyPressed(keyCode: Int)
    fun stop()
}