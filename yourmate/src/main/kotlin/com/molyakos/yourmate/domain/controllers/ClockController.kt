package com.molyakos.yourmate.domain.controllers

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class ClockController : IClockController {

    var disposable: Disposable? = null

    override fun prepare(callback: (String) -> Unit) {
        disposable = Observable.interval(0, 1, TimeUnit.MINUTES, Schedulers.io())
                .subscribe { callback(getCurrentTime()) }
    }

    private fun getCurrentTime(): String {
        return SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date(System.currentTimeMillis()))
    }

    override fun destroy() {
        disposable?.dispose()
    }
}