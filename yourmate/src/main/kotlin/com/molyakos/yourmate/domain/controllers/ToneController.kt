package com.molyakos.yourmate.domain.controllers

import android.media.AudioManager
import android.media.ToneGenerator
import android.view.KeyEvent

class ToneController(private val audioManager: AudioManager) : IToneController {

    private val TONE_LENGTH_MS = 80
    private var toneGenerator: ToneGenerator? = null

    override fun prepare() {
        try {
            toneGenerator = ToneGenerator(AudioManager.STREAM_DTMF, TONE_LENGTH_MS)
        } catch (e: RuntimeException) {
            toneGenerator = null
        }
    }

    override fun keyPressed(keyCode: Int) {
        when (keyCode) {
            KeyEvent.KEYCODE_1 -> playTone(ToneGenerator.TONE_DTMF_1, TONE_LENGTH_MS)
            KeyEvent.KEYCODE_2 -> playTone(ToneGenerator.TONE_DTMF_2, TONE_LENGTH_MS)
            KeyEvent.KEYCODE_3 -> playTone(ToneGenerator.TONE_DTMF_3, TONE_LENGTH_MS)
            KeyEvent.KEYCODE_4 -> playTone(ToneGenerator.TONE_DTMF_4, TONE_LENGTH_MS)
            KeyEvent.KEYCODE_5 -> playTone(ToneGenerator.TONE_DTMF_5, TONE_LENGTH_MS)
            KeyEvent.KEYCODE_6 -> playTone(ToneGenerator.TONE_DTMF_6, TONE_LENGTH_MS)
            KeyEvent.KEYCODE_7 -> playTone(ToneGenerator.TONE_DTMF_7, TONE_LENGTH_MS)
            KeyEvent.KEYCODE_8 -> playTone(ToneGenerator.TONE_DTMF_8, TONE_LENGTH_MS)
            KeyEvent.KEYCODE_9 -> playTone(ToneGenerator.TONE_DTMF_9, TONE_LENGTH_MS)
            KeyEvent.KEYCODE_0 -> playTone(ToneGenerator.TONE_DTMF_0, TONE_LENGTH_MS)
            KeyEvent.KEYCODE_POUND -> playTone(ToneGenerator.TONE_DTMF_P, TONE_LENGTH_MS)
            KeyEvent.KEYCODE_STAR -> playTone(ToneGenerator.TONE_DTMF_S, TONE_LENGTH_MS)
        }
    }

    private fun playTone(tone: Int, durationMs: Int): Boolean =
            when (audioManager.ringerMode) {
                AudioManager.RINGER_MODE_SILENT, AudioManager.RINGER_MODE_VIBRATE -> false
                else -> toneGenerator?.startTone(tone, durationMs) ?: false
            }

    override fun stop() {
        toneGenerator?.release()
        toneGenerator = null
    }
}