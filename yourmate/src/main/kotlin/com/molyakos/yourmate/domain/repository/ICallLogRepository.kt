package com.molyakos.yourmate.domain.repository

import com.molyakos.yourmate.domain.models.CallLogModel
import io.reactivex.Observable

interface ICallLogRepository {
    fun fetchMissedCallsCount(): Observable<Int>
    fun fetchCallLogList(): Observable<List<CallLogModel>>
}