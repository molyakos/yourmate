package com.molyakos.yourmate.domain.models

import android.annotation.SuppressLint
import io.mironov.smuggler.AutoParcelable

@SuppressLint("ParcelCreator")
data class WeatherModel constructor(
        val cityName: String,
        val sunrise: String,
        val sunset: String,
        val temp: String,
        val icon: String,
        val tempUnits: String): AutoParcelable
