package com.molyakos.yourmate.domain.interactors

import com.molyakos.yourmate.domain.models.CallLogModel
import com.molyakos.yourmate.domain.repository.ICallLogRepository
import com.molyakos.yourmate.utils.IRxSchedulers
import io.reactivex.Observable

interface ICallLogInteractor {
    fun updateCallLog() : Observable<List<CallLogModel>>
    fun updateMissedCallCount() : Observable<Int>
}

class CallLogInteractor(private val callLogRepository: ICallLogRepository,
                        private val schedulers: IRxSchedulers): ICallLogInteractor {
    override fun updateCallLog(): Observable<List<CallLogModel>> {
        return callLogRepository.fetchCallLogList().compose(schedulers.applySchedulers())
    }

    override fun updateMissedCallCount(): Observable<Int> {
        return callLogRepository.fetchMissedCallsCount().compose(schedulers.applySchedulers())
    }
}