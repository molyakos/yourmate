package com.molyakos.yourmate.domain.repository

import io.reactivex.Observable

interface ISMSRepository {
    fun fetchMissedSMSCount(): Observable<Int>
}