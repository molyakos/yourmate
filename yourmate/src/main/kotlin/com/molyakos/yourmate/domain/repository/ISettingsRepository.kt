package com.molyakos.yourmate.domain.repository

interface ISettingsRepository {
    fun fetchCurrentCityName(): String
    fun fetchUnits(): String
}