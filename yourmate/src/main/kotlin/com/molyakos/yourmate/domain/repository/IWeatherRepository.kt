package com.molyakos.yourmate.domain.repository

import com.molyakos.yourmate.data.entity.WeatherEntity
import com.molyakos.yourmate.domain.models.WeatherModel
import io.reactivex.Observable

interface IWeatherRepository {
    fun writeWeatherToDiskStore(weatherModel: WeatherEntity)
    fun fetchWeatherFromDiskStore(): Observable<WeatherModel>
    fun fetchWeatherFromCloudStore(cityName: String, units: String): Observable<WeatherModel>
}