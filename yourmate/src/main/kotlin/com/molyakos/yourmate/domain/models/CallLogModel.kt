package com.molyakos.yourmate.domain.models

import android.annotation.SuppressLint
import io.mironov.smuggler.AutoParcelable

@SuppressLint("ParcelCreator")
data class CallLogModel(val name: String) : AutoParcelable {
    lateinit var number: String
    lateinit var type: String
    lateinit var date: String
    lateinit var duration: String
}