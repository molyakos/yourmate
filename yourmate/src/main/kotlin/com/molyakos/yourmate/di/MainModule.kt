package com.molyakos.yourmate.di

import com.molyakos.yourmate.app.DialogManager
import com.molyakos.yourmate.presentation.presenter.module.main.MainPresenter
import com.molyakos.yourmate.presentation.view.module.favorite.FavoriteFragment
import com.molyakos.yourmate.presentation.view.module.home.HomeFragment
import com.molyakos.yourmate.presentation.view.module.main.MainActivity
import com.molyakos.yourmate.presentation.view.module.phone.PhoneFragment

object MainModule {

    private var dialogManager: DialogManager? = null
    private var presenter: MainPresenter? = null
    private var phoneFragment: PhoneFragment? = null
    private var homeFragment: HomeFragment? = null
    private var favoriteFragment: FavoriteFragment? = null

    fun inject(view: MainActivity) {
        dialogManager ?: let { dialogManager = DialogManager() }
        presenter ?: let { presenter = MainPresenter() }
        phoneFragment ?: let { phoneFragment = PhoneFragment() }
        homeFragment ?: let { homeFragment = HomeFragment() }
        favoriteFragment ?: let { favoriteFragment = FavoriteFragment() }

        view.dialogManager = dialogManager!!
        view.presenter = presenter!!
        view.phoneFragment = phoneFragment!!
        view.homeFragment = homeFragment!!
        view.favoriteFragment = favoriteFragment!!
    }

    fun destroy() {
        presenter = null
        dialogManager = null
    }
}