package com.molyakos.yourmate.di

import com.molyakos.yourmate.app.ApplicationInfo
import com.molyakos.yourmate.app.ReportHelper
import com.molyakos.yourmate.presentation.view.module.report.ReportActivity

object ReportModule {

    private var reportHelper: ReportHelper? = null
    private var applicationInfo: ApplicationInfo? = null

    fun inject(view: ReportActivity) {
        val packageInfo = view.packageManager.getPackageInfo(view.packageName, 0);
        applicationInfo = ApplicationInfo(packageInfo.packageName, packageInfo.versionName, packageInfo.versionCode)

        reportHelper ?: let { reportHelper = ReportHelper(applicationInfo!!) }

        view.reportHelper = reportHelper!!
    }

    fun destroy() {
        applicationInfo = null
        reportHelper = null
    }
}