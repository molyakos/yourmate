package com.molyakos.yourmate.di

import android.content.Context
import android.media.AudioManager
import com.molyakos.yourmate.data.repository.ExternalRepository
import com.molyakos.yourmate.data.repository.IExternalRepository
import com.molyakos.yourmate.app.IResourceManager
import com.molyakos.yourmate.app.ResourceManager
import com.molyakos.yourmate.data.entity.mapper.CallLogEntityDataMapper
import com.molyakos.yourmate.domain.controllers.ToneController
import com.molyakos.yourmate.domain.interactors.CallLogInteractor
import com.molyakos.yourmate.data.repository.calllog.CallLogRepository
import com.molyakos.yourmate.data.repository.calllog.datastore.CallLogDiskStore
import com.molyakos.yourmate.domain.controllers.ClockController
import com.molyakos.yourmate.domain.controllers.IClockController
import com.molyakos.yourmate.presentation.presenter.module.phone.PhonePresenter
import com.molyakos.yourmate.presentation.view.module.phone.PhoneFragment
import com.molyakos.yourmate.utils.RxSchedulers

object PhoneModule {

    private var resourceManager: IResourceManager? = null
    private var contentResolverWrapper: IExternalRepository? = null
    private var diskStore: CallLogDiskStore? = null
    private var mapper: CallLogEntityDataMapper? = null
    private var toneController: ToneController? = null
    private var callLogRepository: CallLogRepository? = null
    private var schedulers: RxSchedulers? = null
    private var callLogInteractor: CallLogInteractor? = null
    private var presenter: PhonePresenter? = null

    fun inject(view: PhoneFragment) {
        resourceManager ?: let { resourceManager = ResourceManager(view.activity) }
        contentResolverWrapper ?: let { contentResolverWrapper = ExternalRepository(view.activity.contentResolver) }
        diskStore ?: let { diskStore = CallLogDiskStore(contentResolverWrapper!!) }
        mapper ?: let { mapper = CallLogEntityDataMapper(resourceManager!!) }
        toneController ?: let { toneController = ToneController(view.activity.getSystemService(Context.AUDIO_SERVICE) as AudioManager) }
        callLogRepository ?: let { callLogRepository = CallLogRepository(mapper!!, diskStore!!) }
        schedulers ?: let { schedulers = RxSchedulers() }
        callLogInteractor ?: let { callLogInteractor = CallLogInteractor(callLogRepository!!, schedulers!!) }
        presenter ?: let { presenter = PhonePresenter(toneController!!, callLogInteractor!!) }

        view.presenter = presenter!!
    }

    fun destroy() {
        resourceManager = null
        contentResolverWrapper = null
        diskStore = null
        mapper = null
        toneController = null
        callLogRepository = null
        schedulers = null
        callLogInteractor = null
        presenter = null
    }
}