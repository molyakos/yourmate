package com.molyakos.yourmate.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.molyakos.yourmate.data.WeatherDeserializer
import com.molyakos.yourmate.data.entity.WeatherEntity

object GSONModule {
    fun gsonWeatherDeserializer(tempUnits: String): Gson = GsonBuilder()
            .registerTypeAdapter(WeatherEntity::class.java, WeatherDeserializer(tempUnits))
            .create()
}