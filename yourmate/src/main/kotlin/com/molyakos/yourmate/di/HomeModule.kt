package com.molyakos.yourmate.di

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.molyakos.yourmate.R
import com.molyakos.yourmate.app.*
import com.molyakos.yourmate.data.entity.mapper.CallLogEntityDataMapper
import com.molyakos.yourmate.data.entity.mapper.WeatherEntityDataMapper
import com.molyakos.yourmate.data.net.IRestApi
import com.molyakos.yourmate.data.net.RestApi
import com.molyakos.yourmate.data.repository.ExternalRepository
import com.molyakos.yourmate.data.repository.IExternalRepository
import com.molyakos.yourmate.domain.interactors.CallLogInteractor
import com.molyakos.yourmate.domain.interactors.SMSInteractor
import com.molyakos.yourmate.data.repository.calllog.CallLogRepository
import com.molyakos.yourmate.data.repository.calllog.datastore.CallLogDiskStore
import com.molyakos.yourmate.data.repository.settings.SettingsRepository
import com.molyakos.yourmate.data.repository.settings.datastore.SettingsDiskStore
import com.molyakos.yourmate.data.repository.sms.SMSRepository
import com.molyakos.yourmate.data.repository.sms.datastore.SMSDiskStore
import com.molyakos.yourmate.data.repository.weather.WeatherRepository
import com.molyakos.yourmate.data.repository.weather.datastore.WeatherCloudStore
import com.molyakos.yourmate.data.repository.weather.datastore.WeatherDiskStore
import com.molyakos.yourmate.domain.interactors.SettingsInteractor
import com.molyakos.yourmate.domain.interactors.WeatherInteractor
import com.molyakos.yourmate.app.DateFormatter
import com.molyakos.yourmate.domain.controllers.ClockController
import com.molyakos.yourmate.domain.controllers.IClockController
import com.molyakos.yourmate.presentation.presenter.module.home.HomePresenter
import com.molyakos.yourmate.presentation.presenter.module.home.IHomePresenter
import com.molyakos.yourmate.presentation.view.module.home.HomeFragment
import com.molyakos.yourmate.utils.RxSchedulers

object HomeModule {

    private var gson: Gson? = null
    private var diskWrapper: IDiskIO? = null
    private var diskStore: WeatherDiskStore? = null
    private var weatherCloudStore: WeatherCloudStore? = null
    private var restApi: IRestApi? = null
    private var contentResolverWrapper: IExternalRepository? = null
    private var smsDiskStore: SMSDiskStore? = null
    private var smsRepository: SMSRepository? = null
    private var schedulers: RxSchedulers? = null
    private var smsInteractor: SMSInteractor? = null
    private var dateFormatter: DateFormatter? = null
    private var weatherMapper: WeatherEntityDataMapper? = null
    private var weatherRepository: WeatherRepository? = null
    private var weatherInteractor: WeatherInteractor? = null
    private var resourceManager: ResourceManager? = null
    private var diskCallLogStore: CallLogDiskStore? = null
    private var mapper: CallLogEntityDataMapper? = null
    private var callLogRepository: CallLogRepository? = null
    private var callLogInteractor: CallLogInteractor? = null
    private var sharedPreferences: SharedPreferences? = null
    private var settingsDiskStore: SettingsDiskStore? = null
    private var settingsRepository: SettingsRepository? = null
    private var settingsInteractor: SettingsInteractor? = null
    private var presenter: IHomePresenter? = null
    private var clockController: IClockController? = null

    fun inject(view: HomeFragment) {
        contentResolverWrapper ?: let { contentResolverWrapper = ExternalRepository(view.activity.contentResolver) }
        smsDiskStore ?: let { smsDiskStore = SMSDiskStore(contentResolverWrapper!!) }
        smsRepository ?: let { smsRepository = SMSRepository(smsDiskStore!!) }
        schedulers ?: let { schedulers = RxSchedulers() }
        smsInteractor ?: let { smsInteractor = SMSInteractor(smsRepository!!, schedulers!!) }
        resourceManager ?: let { resourceManager = ResourceManager(view.activity) }
        diskCallLogStore ?: let { diskCallLogStore = CallLogDiskStore(contentResolverWrapper!!) }
        mapper ?: let { mapper = CallLogEntityDataMapper(resourceManager!!) }
        callLogRepository ?: let { callLogRepository = CallLogRepository(mapper!!, diskCallLogStore!!) }
        callLogInteractor ?: let { callLogInteractor = CallLogInteractor(callLogRepository!!, schedulers!!) }
        sharedPreferences ?: let { sharedPreferences = PreferenceManager.getDefaultSharedPreferences(view.activity) }
        settingsDiskStore ?: let { settingsDiskStore = SettingsDiskStore(sharedPreferences!!) }
        settingsRepository ?: let { settingsRepository = SettingsRepository(settingsDiskStore!!) }
        settingsInteractor ?: let { settingsInteractor = SettingsInteractor(settingsRepository!!) }
        dateFormatter ?: let { dateFormatter = DateFormatter() }
        weatherMapper ?: let { weatherMapper = WeatherEntityDataMapper(dateFormatter!!, view.activity.getString(R.string.metric)) }
        gson ?: let { gson = GSONModule.gsonWeatherDeserializer(settingsDiskStore!!.fetchUnits()) }
        diskWrapper ?: let { diskWrapper = DiskIO(view.activity) }
        diskStore ?: let { diskStore = WeatherDiskStore(diskWrapper!!) }
        restApi ?: let { restApi = RestApi() }
        weatherCloudStore ?: let { weatherCloudStore = WeatherCloudStore(restApi!!) }
        weatherRepository ?: let { weatherRepository = WeatherRepository(weatherMapper!!, diskStore!!, weatherCloudStore!!, gson!!, Gson()) }
        weatherInteractor ?: let { weatherInteractor = WeatherInteractor(weatherRepository!!, schedulers!!) }
        presenter ?: let { presenter = HomePresenter(smsInteractor!!, callLogInteractor!!, weatherInteractor!!, settingsInteractor!!) }
        clockController ?: let { clockController = ClockController() }

        view.clockController = clockController!!
        view.presenter = presenter!!
    }

    fun destroy() {
        gson = null
        diskWrapper = null
        diskStore = null
        weatherCloudStore = null
        restApi = null
        contentResolverWrapper = null
        smsDiskStore = null
        smsRepository = null
        schedulers = null
        smsInteractor = null
        dateFormatter = null
        weatherMapper = null
        weatherRepository = null
        weatherInteractor = null
        resourceManager = null
        diskCallLogStore = null
        mapper = null
        callLogRepository = null
        callLogInteractor = null
        sharedPreferences = null
        settingsDiskStore = null
        settingsRepository = null
        settingsInteractor = null
        presenter = null
        clockController = null
    }
}