package com.molyakos.yourmate.di

import com.molyakos.yourmate.data.cache.ImageCache
import com.molyakos.yourmate.presentation.presenter.module.favorite.FavoritePresenter
import com.molyakos.yourmate.presentation.view.module.favorite.FavoriteFragment

object FavoriteModule {

    private var imageCache: ImageCache? = null
    private var presenter: FavoritePresenter? = null

    fun inject(view: FavoriteFragment) {
        imageCache ?: let { imageCache = ImageCache }
        presenter ?: let { presenter = FavoritePresenter(imageCache!!) }

        view.presenter = presenter!!
    }

    fun destroy() {
        presenter = null
        imageCache = null
    }
}