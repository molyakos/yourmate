package com.molyakos.yourmate.di

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.molyakos.yourmate.R
import com.molyakos.yourmate.app.DiskIO
import com.molyakos.yourmate.data.net.RestApi
import com.molyakos.yourmate.data.repository.weather.WeatherRepository
import com.molyakos.yourmate.data.repository.weather.datastore.WeatherCloudStore
import com.molyakos.yourmate.data.repository.weather.datastore.WeatherDiskStore
import com.molyakos.yourmate.domain.interactors.WeatherInteractor
import com.molyakos.yourmate.app.DateFormatter
import com.molyakos.yourmate.app.WeatherUpdateService
import com.molyakos.yourmate.data.entity.mapper.WeatherEntityDataMapper
import com.molyakos.yourmate.data.repository.settings.SettingsRepository
import com.molyakos.yourmate.data.repository.settings.datastore.SettingsDiskStore
import com.molyakos.yourmate.domain.interactors.SettingsInteractor
import com.molyakos.yourmate.utils.RxSchedulers

object WeatherUpdateServiceModule {

    private var schedulers: RxSchedulers? = null
    private var dateFormatter: DateFormatter? = null
    private var diskWrapper: DiskIO? = null
    private var diskStore: WeatherDiskStore? = null
    private var gsonWithDeserializer: Gson? = null
    private var mapper: WeatherEntityDataMapper? = null
    private var weatherRepository: WeatherRepository? = null
    private var sharedPreferences: SharedPreferences? = null
    private var settingsDiskStore: SettingsDiskStore? = null
    private var settingsRepository: SettingsRepository? = null
    private var settingsInteractor: SettingsInteractor? = null
    private var weatherInteractor: WeatherInteractor? = null

    fun inject(context: WeatherUpdateService) {
        schedulers ?: let { schedulers = RxSchedulers() }
        sharedPreferences ?: let { sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context) }
        settingsDiskStore ?: let { settingsDiskStore = SettingsDiskStore(sharedPreferences!!) }
        settingsRepository ?: let { settingsRepository = SettingsRepository(settingsDiskStore!!) }
        settingsInteractor ?: let { settingsInteractor = SettingsInteractor(settingsRepository!!) }
        dateFormatter ?: let { dateFormatter = DateFormatter() }
        diskWrapper ?: let { diskWrapper = DiskIO(context) }
        diskStore ?: let { diskStore = WeatherDiskStore(diskWrapper!!) }
        gsonWithDeserializer ?: let { gsonWithDeserializer = GSONModule.gsonWeatherDeserializer(settingsDiskStore!!.fetchUnits()) }
        mapper ?: let { mapper = WeatherEntityDataMapper(dateFormatter!!, context.getString(R.string.metric)) }
        weatherRepository ?: let { weatherRepository = WeatherRepository(mapper!!, diskStore!!, WeatherCloudStore(RestApi()), gsonWithDeserializer!!, Gson()) }
        weatherInteractor ?: let { weatherInteractor = WeatherInteractor(weatherRepository!!, schedulers!!) }

        context.weatherInteractor = weatherInteractor!!
        context.settingsInteractor = settingsInteractor!!
    }

    fun destroy() {
        schedulers = null
        dateFormatter = null
        diskWrapper = null
        diskStore = null
        gsonWithDeserializer = null
        mapper = null
        weatherRepository = null
        sharedPreferences = null
        settingsDiskStore = null
        settingsRepository = null
        weatherInteractor = null
        settingsInteractor = null
    }
}