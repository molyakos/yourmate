package com.molyakos.yourmate.utils

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.support.graphics.drawable.VectorDrawableCompat

fun pxToDp(px: Int): Int {
    val metrics = Resources.getSystem().displayMetrics
    val dp = px / (metrics.densityDpi / 160f)
    return Math.round(dp)
}

fun getVectorDrawable(context: Context, id: Int): Drawable {
    return VectorDrawableCompat.create(context.resources, id, null)!!
}