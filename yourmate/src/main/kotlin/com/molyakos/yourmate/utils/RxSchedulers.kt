package com.molyakos.yourmate.utils

import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface IRxSchedulers {
    fun <T> applySchedulers(): ObservableTransformer<T, T>
}

class RxSchedulers : IRxSchedulers {
    override fun <T> applySchedulers(): ObservableTransformer<T, T> {
        return ObservableTransformer<T, T> { upstream ->
            upstream.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
        }
    }
}