package com.molyakos.yourmate.presentation.view.module.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.molyakos.yourmate.R
import com.molyakos.yourmate.di.FavoriteModule
import com.molyakos.yourmate.presentation.presenter.module.favorite.FavoritePresenter
import com.molyakos.yourmate.presentation.presenter.module.favorite.IFavoritePresenter
import com.molyakos.yourmate.presentation.view.BaseFragment
import com.molyakos.yourmate.presentation.view.module.main.IMainView

class FavoriteFragment: BaseFragment(), IFavoriteView {

    lateinit var presenter: IFavoritePresenter

    override fun initDependencies(hostView: IMainView) = FavoriteModule.inject(this)

    override fun destroyDependencies() = FavoriteModule.destroy()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.favorite_fragment, container, false)
        return view
    }

    override fun onStart() {
        super.onStart()

        presenter.bind(this)
    }

    override fun onStop() {
        super.onStop()

        presenter.unbind()
    }
}