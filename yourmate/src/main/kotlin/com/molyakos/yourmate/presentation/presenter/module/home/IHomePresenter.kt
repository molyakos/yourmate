package com.molyakos.yourmate.presentation.presenter.module.home

import com.molyakos.yourmate.presentation.view.module.home.IHomeView

interface IHomePresenter {
    fun bind(homeView: IHomeView)
    fun unbind()
    fun updateMissedSMSCount()
    fun updateMissedCallsCount()
    fun updateWeather()
    fun updateWeather(cityName: String?, units: String?)
}