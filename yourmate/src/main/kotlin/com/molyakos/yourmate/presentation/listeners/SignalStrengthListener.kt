package com.molyakos.yourmate.presentation.listeners

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.support.graphics.drawable.VectorDrawableCompat
import android.telephony.PhoneStateListener
import android.telephony.SignalStrength

class SignalStrengthListener(private val mContext: Context) : PhoneStateListener() {

    private var strength = 4
    private val view: View

    interface View {
        fun updateNetworkDrawable(drawable: Drawable?)
    }

    init {
        view = mContext as View
    }

    override fun onSignalStrengthsChanged(signalstrength: SignalStrength) {
        super.onSignalStrengthsChanged(signalstrength)
        val signalStrength = signalstrength.gsmSignalStrength
        when (signalStrength) {
            in 31..signalStrength -> strength = 4
            in 21..31 -> strength = 3
            in 13..21 -> strength = 2
            in 6..13 -> strength = 1
            in signalStrength..6 -> strength = 0
            99 -> strength = 0
        }
        updateNetworkDrawable()
    }

    fun updateNetworkDrawable() {
        val type: String
        if (isNetwork()) {
            type = ""
        } else {
            type = "connected_no_internet_"
        }
        if (strength > 4 || strength < 0) {
            strength = 4
        }
        val icon = StringBuilder().append("ic_signal_cellular_").append(type).append(strength).append("_bar_white_24dp").toString()
        val id = mContext.resources.getIdentifier(icon, "drawable", mContext.packageName)
        val drawable = VectorDrawableCompat.create(mContext.resources, id, null)
        view.updateNetworkDrawable(drawable)
    }

    fun isNetwork(): Boolean {
        val connectivityManager = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnectedOrConnecting
    }
}