package com.molyakos.yourmate.presentation.presenter.module.favorite

import com.molyakos.yourmate.presentation.view.module.favorite.IFavoriteView

interface IFavoritePresenter{
    fun bind(favoriteView: IFavoriteView)
    fun unbind()
}