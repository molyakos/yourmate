package com.molyakos.yourmate.presentation.view

import com.trello.rxlifecycle2.LifecycleProvider
import com.trello.rxlifecycle2.android.FragmentEvent

interface IBaseFragment : LifecycleProvider<FragmentEvent> {
}