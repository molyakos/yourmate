package com.molyakos.yourmate.presentation.presenter.module.home

import com.molyakos.yourmate.domain.interactors.*
import com.molyakos.yourmate.presentation.view.IBaseFragment
import com.molyakos.yourmate.presentation.view.module.home.IHomeView
import com.trello.rxlifecycle2.kotlin.bindToLifecycle

class HomePresenter(private val smsInteractor: ISMSInteractor,
                    private val callLogInteractor: ICallLogInteractor,
                    private val weatherInteractor: IWeatherInteractor,
                    private val settingsInteractor: ISettingsInteractor) : IHomePresenter {

    private var view: IHomeView? = null
    private var androidView: IBaseFragment? = null

    override fun bind(homeView: IHomeView) {
        view = homeView
        androidView = homeView as IBaseFragment
    }

    override fun unbind() {
        view = null
        androidView = null
    }

    override fun updateWeather(cityName: String?, units: String?) {
        var currentCityName = cityName
        currentCityName ?: let { currentCityName = settingsInteractor.fetchCurrentCityName() }

        var currentTempUnits = units
        currentTempUnits ?: let { currentTempUnits = settingsInteractor.fetchUnits() }

        weatherInteractor.updateWeather(currentCityName!!, currentTempUnits!!)
                .bindToLifecycle(androidView!!)
                .subscribe({
                    view?.updateWeather(it)
                }, {
                    it.printStackTrace()
                    view?.errorUpdateWeather()
                })
    }

    override fun updateWeather() {
        updateWeather(null, null)
    }

    override fun updateMissedSMSCount() {
        smsInteractor.updateMissedSMSCount()
                .bindToLifecycle(androidView!!)
                .subscribe({ view?.updateMissedSMSCount(it) })
    }

    override fun updateMissedCallsCount() {
        callLogInteractor.updateMissedCallCount()
                .bindToLifecycle(androidView!!)
                .subscribe({ view?.updateMissedCallsCount(it) })
    }
}