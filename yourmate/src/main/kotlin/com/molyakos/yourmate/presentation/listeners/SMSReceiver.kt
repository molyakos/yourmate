package com.molyakos.yourmate.presentation.listeners

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class SMSReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        context.sendBroadcast(Intent("com.molyakos.yourmate.SMS_RECEIVED"))
    }
}