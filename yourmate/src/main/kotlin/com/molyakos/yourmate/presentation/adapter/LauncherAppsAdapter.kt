package com.molyakos.yourmate.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.molyakos.yourmate.R
import com.molyakos.yourmate.presentation.models.ShortCutModel

class LauncherAppsAdapter(private val mContext: Context,
                          layoutResource: Int, textViewResourceId: Int,
                          private val appList: List<ShortCutModel>) : ArrayAdapter<ShortCutModel>(mContext, layoutResource, textViewResourceId, appList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var view = convertView
        val holder: ViewHolder
        val app = appList[position]

        if (view == null) {
            val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.select_dialog_item, parent, false)

            holder = ViewHolder(view)
            view.tag = holder
        } else {
            holder = view.tag as ViewHolder
        }

        holder.appNameTextView.text = app.name

        return view
    }

    internal class ViewHolder(view: View) {
        val appNameTextView: TextView = view.findViewById(R.id.app_name) as TextView
    }
}
