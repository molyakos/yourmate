package com.molyakos.yourmate.presentation.presenter.module.main

import com.molyakos.yourmate.presentation.view.module.main.IMainView

class MainPresenter : IMainPresenter {

    private var view: IMainView? = null

    override fun bind(iMainView: IMainView) {
        view = iMainView
    }

    override fun unbind() {
        view = null
    }
}