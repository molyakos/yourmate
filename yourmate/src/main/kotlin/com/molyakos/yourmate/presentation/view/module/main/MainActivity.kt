package com.molyakos.yourmate.presentation.view.module.main

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Fragment
import android.content.*
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.molyakos.yourmate.presentation.view.BaseActivity
import com.molyakos.yourmate.R
import com.molyakos.yourmate.di.MainModule
import com.molyakos.yourmate.presentation.models.ShortCutModel
import com.molyakos.yourmate.app.IDialogManager
import com.molyakos.yourmate.app.PermissionAlertDialog
import com.molyakos.yourmate.presentation.adapter.LauncherAppsAdapter
import com.molyakos.yourmate.presentation.listeners.*
import com.molyakos.yourmate.presentation.presenter.module.main.IMainPresenter
import com.molyakos.yourmate.presentation.view.module.favorite.FavoriteFragment
import com.molyakos.yourmate.presentation.view.module.home.*
import com.molyakos.yourmate.presentation.view.module.phone.PhoneFragment
import com.molyakos.yourmate.presentation.view.module.report.ReportActivity
import com.molyakos.yourmate.presentation.view.module.settings.SettingsActivity
import com.molyakos.yourmate.utils.getVectorDrawable

class MainActivity : BaseActivity(), IMainView,
        BatteryReceiver.View, WiFiStrengthReceiver.View, SignalStrengthListener.View {

    private val PERMISSIONS_REQUEST = 1
    private val PERMISSION_REQUEST_PHONE_FRAGMENT = 2
    private val PERMISSION_REQUEST_SCAN_WIFI_STRENGTH = 3
    private val PERMISSION_REQUEST_READ_SMS_MISSED_COUNT = 4
    private val PERMISSION_REQUEST_READ_CALL_MISSED_COUNT = 5
    private val CHANGE_SETTINGS_REQUEST = 6

    private val batteryLevelReceiver = BatteryReceiver()
    private var wiFiStrengthReceiver: WiFiStrengthReceiver? = null

    lateinit var dialogManager: IDialogManager
    lateinit var presenter: IMainPresenter
    lateinit var phoneFragment: PhoneFragment
    lateinit var homeFragment: HomeFragment
    lateinit var favoriteFragment: FavoriteFragment

    private lateinit var drawerLayout: DrawerLayout
    private lateinit var bottomNavigationView: BottomNavigationView
    private lateinit var batteryTextView: TextView
    private lateinit var batteryImageView: ImageView
    private lateinit var networkImageView: ImageView
    private lateinit var wifiImageView: ImageView

    private val onClickListener = View.OnClickListener {
        when (it.id) {
            R.id.menu_image -> drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_phone -> {
                if (bottomNavigationView.selectedItemId == item.itemId) {
                    if (phoneFragment.isAdded) {
                        phoneFragment.sendPhoneAction()
                        return@OnNavigationItemSelectedListener true
                    }
                } else {
                    if (!(ActivityCompat.checkSelfPermission(this@MainActivity, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(this@MainActivity, Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(this@MainActivity, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED)) {
                        ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.READ_CALL_LOG, Manifest.permission.CALL_PHONE, Manifest.permission.READ_CONTACTS), PERMISSION_REQUEST_PHONE_FRAGMENT)
                        return@OnNavigationItemSelectedListener false
                    } else {
                        loadFragment(phoneFragment)
                        return@OnNavigationItemSelectedListener true
                    }
                }
            }
            R.id.navigation_home -> {
                loadFragment(homeFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_favorites -> {
                loadFragment(favoriteFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private val navigationOnClickListener = NavigationView.OnNavigationItemSelectedListener {
        when (it.itemId) {
            R.id.nav_feedback -> startActivity(Intent(this, ReportActivity::class.java))
            R.id.nav_settings -> startActivityForResult(Intent(this, SettingsActivity::class.java), CHANGE_SETTINGS_REQUEST)
            R.id.nav_info -> showInfoAppDialog()
            R.id.nav_exit -> exitApp()
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        true
    }

    override fun initDependencies() = MainModule.inject(this)

    override fun destroyDependencies() = MainModule.destroy()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navView = findViewById(R.id.nav_view) as NavigationView
        navView.setNavigationItemSelectedListener(navigationOnClickListener)

        drawerLayout = findViewById(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        val menuImageView = findViewById(R.id.menu_image) as ImageView
        menuImageView.setImageDrawable(getVectorDrawable(this, R.drawable.ic_menu_white_24dp))
        menuImageView.setOnClickListener(onClickListener)
        batteryTextView = findViewById(R.id.battery_text) as TextView
        batteryImageView = findViewById(R.id.battery_image) as ImageView
        wifiImageView = findViewById(R.id.wifi_image) as ImageView
        networkImageView = findViewById(R.id.network_image) as ImageView
        bottomNavigationView = findViewById(R.id.navigation) as BottomNavigationView
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        bottomNavigationView.selectedItemId = R.id.navigation_home

        if (!(ActivityCompat.checkSelfPermission(this@MainActivity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this@MainActivity, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this@MainActivity, Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_SMS, Manifest.permission.READ_CALL_LOG), PERMISSIONS_REQUEST)
        }

        restoreInstance(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        registerReceivers()

        val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        telephonyManager.listen(SignalStrengthListener(this), PhoneStateListener.LISTEN_SIGNAL_STRENGTHS)

        presenter.bind(this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("selectedItemId", bottomNavigationView.selectedItemId)
    }

    override fun onStop() {
        super.onStop()
        unregisterReceivers()
        presenter.unbind()
    }

    private fun restoreInstance(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            val selectedItemId = it.getInt("selectedItemId")
            bottomNavigationView.selectedItemId = selectedItemId
        }
    }

    private fun registerReceivers() {
        registerReceiver(batteryLevelReceiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
        if (ActivityCompat.checkSelfPermission(this@MainActivity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            registerWiFiStrengthReceiver()
        }
    }

    private fun registerWiFiStrengthReceiver() {
        val intentFilterWifi = IntentFilter()
        intentFilterWifi.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        intentFilterWifi.addAction(WifiManager.RSSI_CHANGED_ACTION)
        intentFilterWifi.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION)
        intentFilterWifi.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION)
        wiFiStrengthReceiver = WiFiStrengthReceiver()
        registerReceiver(wiFiStrengthReceiver, intentFilterWifi)
    }

    private fun unregisterReceivers() {
        unregisterReceiver(batteryLevelReceiver)
        wiFiStrengthReceiver?.let {
            unregisterReceiver(wiFiStrengthReceiver)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CHANGE_SETTINGS_REQUEST && resultCode == Activity.RESULT_OK) {
            val cityName = data?.getStringExtra(SettingsActivity.CITY_NAME)
            val units = data?.getStringExtra(SettingsActivity.UNITS)
            homeFragment.updateWeather(cityName, units)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isEmpty()) return

        if (requestCode == PERMISSIONS_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                registerWiFiStrengthReceiver()
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    val dialog = PermissionAlertDialog(this, R.string.attention_label, R.string.wifi_permission_message)
                    dialog.setPositiveButton(R.string.allow, DialogInterface.OnClickListener { _, _ ->
                        ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), PERMISSION_REQUEST_SCAN_WIFI_STRENGTH)
                    })
                    dialogManager.add(dialog)
                }
            }

            if (grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                homeFragment.updateMissedSMSCount()
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, Manifest.permission.READ_SMS)) {
                    val dialog = PermissionAlertDialog(this, R.string.attention_label, R.string.sms_permission_message)
                    dialog.setPositiveButton(R.string.allow, DialogInterface.OnClickListener { _, _ ->
                        ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.READ_SMS), PERMISSION_REQUEST_READ_SMS_MISSED_COUNT)
                    })
                    dialogManager.add(dialog)
                }
            }

            if (grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                homeFragment.updateMissedCallsCount()
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, Manifest.permission.READ_CALL_LOG)) {
                    val dialog = PermissionAlertDialog(this, R.string.attention_label, R.string.phone_permission_message)
                    dialog.setPositiveButton(R.string.allow, DialogInterface.OnClickListener { _, _ ->
                        ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.READ_CALL_LOG), PERMISSION_REQUEST_READ_CALL_MISSED_COUNT)
                    })
                    dialogManager.add(dialog)
                }
            }
            dialogManager.showAll()
        } else if (requestCode == PERMISSION_REQUEST_PHONE_FRAGMENT) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                bottomNavigationView.selectedItemId = R.id.navigation_phone
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, Manifest.permission.READ_CONTACTS) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, Manifest.permission.READ_CALL_LOG) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, Manifest.permission.CALL_PHONE)) {
                    val dialog = PermissionAlertDialog(this, R.string.attention_label, R.string.phone_permission_message)
                    dialog.setPositiveButton(R.string.allow, DialogInterface.OnClickListener { _, _ ->
                        ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.READ_CONTACTS, Manifest.permission.READ_CALL_LOG, Manifest.permission.CALL_PHONE), PERMISSION_REQUEST_PHONE_FRAGMENT)
                    })
                    dialogManager.insert(dialog)
//                    dialogManager.showAll()
                } else {
                    val dialog = PermissionAlertDialog(this, R.string.attention_label, R.string.get_permission_in_settings)
                    dialog.setPositiveButton(android.R.string.ok, null)
                    dialog.setNegativeButton(null)
                    dialog.show()
                }
            }
        } else if (requestCode == PERMISSION_REQUEST_SCAN_WIFI_STRENGTH) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                registerWiFiStrengthReceiver()
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    val dialog = PermissionAlertDialog(this, R.string.attention_label, R.string.wifi_permission_message)
                    dialog.setPositiveButton(R.string.allow, DialogInterface.OnClickListener { _, _ ->
                        ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), PERMISSION_REQUEST_SCAN_WIFI_STRENGTH)
                    })
                    dialogManager.insert(dialog)
//                    dialogManager.showAll()
                }
            }
        } else if (requestCode == PERMISSION_REQUEST_READ_SMS_MISSED_COUNT) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                homeFragment.updateMissedSMSCount()
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, Manifest.permission.READ_SMS)) {
                    val dialog = PermissionAlertDialog(this, R.string.attention_label, R.string.sms_permission_message)
                    dialog.setPositiveButton(R.string.allow, DialogInterface.OnClickListener { _, _ ->
                        ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.READ_SMS), PERMISSION_REQUEST_READ_SMS_MISSED_COUNT)
                    })
                    dialogManager.insert(dialog)
//                    dialogManager.showAll()
                }
            }
        } else if (requestCode == PERMISSION_REQUEST_READ_CALL_MISSED_COUNT) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                homeFragment.updateMissedCallsCount()
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, Manifest.permission.READ_CALL_LOG)) {
                    val dialog = PermissionAlertDialog(this, R.string.attention_label, R.string.phone_permission_message)
                    dialog.setPositiveButton(R.string.allow, DialogInterface.OnClickListener { _, _ ->
                        ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.READ_CALL_LOG), PERMISSION_REQUEST_READ_CALL_MISSED_COUNT)
                    })
                    dialogManager.insert(dialog)
//                    dialogManager.showAll()
                }
            }
        }
    }

    private fun loadFragment(fragment: Fragment) {
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.animator.show_fragment, R.animator.hide_fragment)
                .replace(R.id.fragment_frame, fragment)
                .commit()
    }

    override fun loadFragment(fragmentType: IMainView.FragmentType) {
        val fragment: Fragment
        if (fragmentType == IMainView.FragmentType.PHONE) {
            fragment = phoneFragment
            fragment.activity?.let { return }
            bottomNavigationView.selectedItemId = R.id.navigation_phone
        } else if (fragmentType == IMainView.FragmentType.HOME) {
            fragment = homeFragment
            fragment.activity?.let { return }
            bottomNavigationView.selectedItemId = R.id.navigation_home
        } else {
            fragment = favoriteFragment
            fragment.activity?.let { return }
            bottomNavigationView.selectedItemId = R.id.navigation_favorites
        }
        loadFragment(fragment)
    }

    override fun updateCallsCount(count: Int) {
        homeFragment.updateMissedCallsCount(count)
    }

    override fun updateSMSCount(count: Int) {
        homeFragment.updateMissedSMSCount(count)
    }

    override fun updateBatteryDrawable(drawable: Drawable) {
        batteryImageView.setImageDrawable(drawable)
    }

    override fun updateBatteryPercent(level: Int) {
        batteryTextView.text = "$level%"
    }

    override fun updateWifiDrawable(drawable: Drawable?) {
        if (drawable == null) {
            wifiImageView.setImageDrawable(null)
            wifiImageView.visibility = View.GONE
            return
        }
        wifiImageView.setImageDrawable(drawable)
        wifiImageView.visibility = View.VISIBLE
    }

    override fun updateNetworkDrawable(drawable: Drawable?) {
        networkImageView.setImageDrawable(drawable)
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            exitApp()
        }
    }

    private fun exitApp() {
        val launcherIntent = Intent(Intent.ACTION_MAIN, null)
        launcherIntent.addCategory(Intent.CATEGORY_HOME)
        val launcherApps = packageManager.queryIntentActivities(launcherIntent, PackageManager.GET_META_DATA)
        val apps = ArrayList<ShortCutModel>(launcherApps.size)
        for (resolveInfo in launcherApps) {
            val app = ShortCutModel(resolveInfo.loadLabel(packageManager) as String, resolveInfo.activityInfo.packageName, null)
            apps.add(app)
        }

        val thisApp = ShortCutModel(getString(R.string.app_name), packageName, null)
        apps.remove(thisApp)

        if (apps.size == 1) {
            val intentToResolve = Intent(Intent.ACTION_MAIN)
            intentToResolve.addCategory(Intent.CATEGORY_HOME)
            intentToResolve.`package` = apps[0].path
            val resolveInfo = packageManager.resolveActivity(intentToResolve, 0)
            val intent = Intent(intentToResolve)
            intent.setClassName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name)
            intent.action = Intent.ACTION_MAIN
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        } else {
            val justExit = ShortCutModel(getString(R.string.exit_app), null, null)
            apps.add(justExit)
            showLauncherAppsDialog(this, apps)
        }
    }

    private fun showLauncherAppsDialog(activity: Activity, apps: List<ShortCutModel>) {
        val appsAdapter = LauncherAppsAdapter(activity, R.layout.select_dialog_item, R.id.app_name, apps)
        val alertDialog = AlertDialog.Builder(activity)
        alertDialog.setAdapter(appsAdapter) { dialog, which ->
            val appName = appsAdapter.getItem(which).name
            if (appName == activity.getString(R.string.exit_app)) {
                System.exit(0)
            }

            for (app in apps) {
                val intentToResolve = Intent(Intent.ACTION_MAIN)
                intentToResolve.addCategory(Intent.CATEGORY_HOME)
                intentToResolve.`package` = app.path
                val resolveInfo = activity.packageManager.resolveActivity(intentToResolve, 0)

                if (appName == app.name) {
                    if (resolveInfo != null) {
                        val intent = Intent(intentToResolve)
                        intent.setClassName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name)
                        intent.action = Intent.ACTION_MAIN
                        intent.addCategory(Intent.CATEGORY_HOME)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        activity.startActivity(intent)
                        System.exit(0)
                    }
                }
            }
            dialog.dismiss()
        }
        alertDialog.create().show()
    }

    private fun showInfoAppDialog() {
        AlertDialog.Builder(this)
                .setTitle("${getString(R.string.app_name)} ${getString(R.string.version_applicaiton)}")
                .setMessage(getString(R.string.info_about_application))
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }
}
