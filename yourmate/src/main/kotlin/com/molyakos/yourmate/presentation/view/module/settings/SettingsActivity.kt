package com.molyakos.yourmate.presentation.view.module.settings

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceActivity
import com.molyakos.yourmate.R
import android.preference.PreferenceFragment
import android.preference.PreferenceManager
import android.content.SharedPreferences
import android.preference.Preference
import android.preference.Preference.OnPreferenceClickListener
import android.provider.Settings

class SettingsActivity : PreferenceActivity(), SharedPreferences.OnSharedPreferenceChangeListener {

    companion object {
        val CITY_NAME = "city_name"
        val UNITS = "temp_units"
    }

    var cityName: String? = null
    var units: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentManager.beginTransaction().replace(android.R.id.content, MainPreference()).commit()

        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()

        val intent = Intent()
        val bundle = Bundle()
        bundle.putString("hoho", "hehe")
        intent.putExtras(bundle)
//        intent.putExtra(cityName, SettingsActivity.CITY_NAME)
//        intent.putExtra(units, SettingsActivity.UNITS)
        setResult(Activity.RESULT_OK, intent)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        sharedPreferences?.let {
            val cityName = it.getString(SettingsActivity.CITY_NAME, null)
            cityName?.let { this.cityName = it }

            val tempUnits = it.getString(SettingsActivity.UNITS, null)
            tempUnits?.let { units = it }
        }
    }

    class MainPreference : PreferenceFragment() {

        private val mOnPreferenceClickListener = object: OnPreferenceClickListener {
            override fun onPreferenceClick(preference: Preference): Boolean {
                if (preference.key == "text_size") {
                    startActivity(Intent(Settings.ACTION_DISPLAY_SETTINGS))
                } else if (preference.key == "accessibility") {
                    startActivity(Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS))
                }
                return true
            }
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.preferences_weather)

            val textSize = findPreference("text_size") as Preference
            textSize.onPreferenceClickListener = mOnPreferenceClickListener

            val accessibility = findPreference("accessibility") as Preference
            accessibility.onPreferenceClickListener = mOnPreferenceClickListener
        }
    }
}