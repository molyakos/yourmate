package com.molyakos.yourmate.presentation.view.module.home

import com.molyakos.yourmate.domain.models.WeatherModel

interface IHomeView {
    fun updateMissedCallsCount(count: Int)
    fun updateMissedSMSCount(count: Int)
    fun updateWeather(model: WeatherModel)
    fun errorUpdateWeather()
}