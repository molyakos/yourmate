package com.molyakos.yourmate.presentation.listeners

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.drawable.Drawable
import android.os.BatteryManager
import android.support.graphics.drawable.VectorDrawableCompat

class BatteryReceiver : BroadcastReceiver() {

    interface View {
        fun updateBatteryDrawable(drawable: Drawable)
        fun updateBatteryPercent(level: Int)
    }

    private var level = -1
    private lateinit var view: View

    override fun onReceive(context: Context, intent: Intent) {
        if (context !is View) {
            // on api <21 may receive ReceiverRestrictedContext instead resourceManager
            return
        }
        view = context
        updateBatteryPercentage(intent)
        setBatteryDrawable(context)
    }

    fun updateBatteryPercentage(intent: Intent) {
        level = intent.getIntExtra("level", 0)
        view.updateBatteryPercent(level)
    }

    fun setBatteryDrawable(context: Context) {
        var percent: String
        val type: String
        val drawable: Drawable
        if (isCharging(context)) {
            type = "ic_battery_charging_"
        } else {
            type = "ic_battery_"
        }
        when (level) {
            in 95..100 -> percent = "full"
            in 85..94 -> percent = 90.toString()
            in 75..84 -> percent = 80.toString()
            in 55..74 -> percent = 60.toString()
            in 45..54 -> percent = 50.toString()
            in 21..44 -> percent = 30.toString()
            else -> percent = 20.toString()
        }

        percent = StringBuilder().append(type).append(percent).append("_white_24dp").toString()
        val id = context.resources.getIdentifier(percent, "drawable", context.packageName)
        drawable = VectorDrawableCompat.create(context.resources, id, null)!!
        view.updateBatteryDrawable(drawable)
    }

    fun isCharging(context: Context): Boolean {
        val intent = context.registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED)) ?: return false
        var isCharging = false
        val status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1) == 2
        val pluggedType = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1)
        val isAllowedType = pluggedType == BatteryManager.BATTERY_PLUGGED_AC ||
                pluggedType == BatteryManager.BATTERY_PLUGGED_USB ||
                pluggedType == BatteryManager.BATTERY_PLUGGED_WIRELESS
        if (status || isAllowedType) {
            isCharging = true
        }
        return isCharging
    }
}