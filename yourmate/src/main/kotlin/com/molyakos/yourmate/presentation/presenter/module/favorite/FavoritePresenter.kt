package com.molyakos.yourmate.presentation.presenter.module.favorite

import com.molyakos.yourmate.data.cache.ImageCache
import com.molyakos.yourmate.presentation.view.module.favorite.IFavoriteView
import com.molyakos.yourmate.presentation.view.module.main.IMainView

class FavoritePresenter(private val imageCache: ImageCache) : IFavoritePresenter {

    private var view: IFavoriteView? = null

    override fun bind(favoriteView: IFavoriteView) {
        view = favoriteView
    }

    override fun unbind() {
        view = null
    }
}