package com.molyakos.yourmate.presentation.presenter.module.phone

import com.molyakos.yourmate.presentation.view.module.phone.IPhoneView

interface IPhonePresenter {
    fun bind(phoneView: IPhoneView)
    fun unbind()
    fun updateCallLog()
    fun stopToneGenerator()
    fun keyPressed(keyCode: Int)
    fun prepareToneGenerator()
}