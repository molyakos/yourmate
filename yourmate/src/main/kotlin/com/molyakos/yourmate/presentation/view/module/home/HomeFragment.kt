package com.molyakos.yourmate.presentation.view.module.home

import android.Manifest
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.ContentObserver
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.os.Messenger
import android.provider.CallLog
import android.provider.Telephony
import android.support.v4.app.ActivityCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.molyakos.yourmate.R
import com.molyakos.yourmate.app.WeatherUpdateService
import com.molyakos.yourmate.di.HomeModule
import com.molyakos.yourmate.domain.controllers.IClockController
import com.molyakos.yourmate.domain.models.WeatherModel
import com.molyakos.yourmate.presentation.presenter.module.home.IHomePresenter
import com.molyakos.yourmate.presentation.view.BaseFragment
import com.molyakos.yourmate.presentation.view.module.main.IMainView
import com.trello.rxlifecycle2.LifecycleTransformer
import com.trello.rxlifecycle2.android.ActivityEvent
import io.reactivex.Observable
import java.util.concurrent.TimeUnit


class HomeFragment : BaseFragment(), IHomeView {

    companion object {
        val MESSENGER_INTENT_KEY = "messenger_activity"
    }

    private val WEATHER_UPDATE_JOB_ID = 32

    private val mHandler = object : Handler() {
        override fun handleMessage(msg: Message?) {
            when (msg?.what) {
                1 -> {
                    val message = msg.obj as WeatherModel
                    updateWeather(message)
                }
            }
        }
    }

    lateinit var presenter: IHomePresenter
    lateinit var clockController: IClockController
    private lateinit var weatherIconImageView: ImageView
    private lateinit var currentTimeTextView: TextView
    private lateinit var currentTempTextView: TextView
    private lateinit var sunsetTextView: TextView
    private lateinit var sunriseTextView: TextView
    private lateinit var missedCallsTextView: TextView
    private lateinit var missedMessagesTextView: TextView

    private val onClickListener = View.OnClickListener { v ->
        when (v.id) {
            R.id.missed_messages_text_view -> {
                sendIntentMessageApp()
            }
            R.id.missed_calls_text_view -> {
                hostView.loadFragment(IMainView.FragmentType.PHONE)
            }
        }
    }

    private val smsContentObserver = object : ContentObserver(null) {
        override fun onChange(selfChange: Boolean) {
            this.onChange(selfChange, null)
        }

        override fun onChange(selfChange: Boolean, uri: Uri?) {
            presenter.updateMissedSMSCount()
        }
    }

    private val callsContentObserver = object : ContentObserver(null) {
        override fun onChange(selfChange: Boolean) {
            this.onChange(selfChange, null)
        }

        override fun onChange(selfChange: Boolean, uri: Uri?) {
            presenter.updateMissedCallsCount()
        }
    }

    override fun initDependencies(hostView: IMainView) = HomeModule.inject(this)

    override fun destroyDependencies() = HomeModule.destroy()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.home_fragment, container, false)

        currentTimeTextView = view.findViewById(R.id.current_time) as TextView
        weatherIconImageView = view.findViewById(R.id.weather_icon) as ImageView
        currentTempTextView = view.findViewById(R.id.current_temp) as TextView
        sunsetTextView = view.findViewById(R.id.sunset_text) as TextView
        sunriseTextView = view.findViewById(R.id.sunrise_text) as TextView
        missedCallsTextView = view.findViewById(R.id.missed_calls_text_view) as TextView
        missedCallsTextView.setOnClickListener(onClickListener)
        missedCallsTextView.visibility = View.GONE
        missedMessagesTextView = view.findViewById(R.id.missed_messages_text_view) as TextView
        missedMessagesTextView.setOnClickListener(onClickListener)
        missedMessagesTextView.visibility = View.GONE

        return view
    }

    override fun onStart() {
        super.onStart()
        registerObservers()

        clockController.prepare { activity.runOnUiThread { currentTimeTextView.setText(it) } }

        startWeatherUpdateService()

        presenter.bind(this)
        presenter.updateWeather()
    }

    override fun onResume() {
        super.onResume()
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
            updateMissedSMSCount()
        }

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {
            presenter.updateMissedCallsCount()
        }
    }

    override fun onStop() {
        super.onStop()
        unregisterObservers()
        clockController.destroy()
        presenter.unbind()
    }

    fun startWeatherUpdateService() {
        val job = JobInfo.Builder(WEATHER_UPDATE_JOB_ID, ComponentName(activity, WeatherUpdateService::class.java))
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .setPeriodic(TimeUnit.HOURS.toMillis(3))
                .build()

        val jobScheduler = activity.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        jobScheduler.schedule(job)

        val startServiceIntent = Intent(activity, WeatherUpdateService::class.java)
        val messengerIncoming = Messenger(mHandler)
        startServiceIntent.putExtra(MESSENGER_INTENT_KEY, messengerIncoming)
        activity.startService(startServiceIntent)
    }

    private fun registerObservers() {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
            activity.contentResolver.registerContentObserver(Telephony.Sms.CONTENT_URI, true, smsContentObserver)
        }

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {
            activity.contentResolver.registerContentObserver(CallLog.Calls.CONTENT_URI, true, callsContentObserver)
        }
    }

    private fun unregisterObservers() {
        activity.contentResolver.unregisterContentObserver(callsContentObserver)
        activity.contentResolver.unregisterContentObserver(smsContentObserver)
    }

    fun updateMissedCallsCount() {
        presenter.updateMissedCallsCount()
    }

    override fun updateMissedCallsCount(count: Int) {
        if (!isAdded) {
            return
        }
        if (count == 0) {
            missedCallsTextView.visibility = View.GONE
            return
        }
        missedCallsTextView.visibility = View.VISIBLE
        missedCallsTextView.text = getString(R.string.missed_calls, count)
    }

    fun updateMissedSMSCount() {
        presenter.updateMissedSMSCount()
    }

    override fun updateMissedSMSCount(count: Int) {
        if (!isAdded) {
            return
        }
        if (count == 0) {
            missedMessagesTextView.visibility = View.GONE
            return
        }
        missedMessagesTextView.visibility = View.VISIBLE
        missedMessagesTextView.text = getString(R.string.missed_message, count)
    }

    private fun sendIntentMessageApp() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_APP_MESSAGING)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    fun updateWeather(cityName: String?, units: String?) {
        presenter.updateWeather(cityName, units)
    }

    override fun updateWeather(model: WeatherModel) {
        if (!isAdded) {
            return
        }
        var idIcon = model.icon

        when (idIcon) {
            "01d" -> idIcon = "day01"
            "02d" -> idIcon = "day02"
            "10d" -> idIcon = "day10"
            "01n" -> idIcon = "night01"
            "02n" -> idIcon = "night02"
            "10n" -> idIcon = "night10"
            "03d", "03n" -> idIcon = "day_night03"
            "04d", "04n" -> idIcon = "day_night04"
            "09d", "09n" -> idIcon = "day_night09"
            "11d", "11n" -> idIcon = "day_night11"
            "13d", "13n" -> idIcon = "day_night13"
            "50d", "50n" -> idIcon = "day_night50"
        }

        val id = resources.getIdentifier(idIcon, "drawable", activity.packageName)
        weatherIconImageView.setImageResource(id)
        currentTempTextView.text = "${model.temp} °${model.tempUnits}"
        sunsetTextView.text = model.sunset
        sunriseTextView.text = model.sunrise
    }

    override fun errorUpdateWeather() {
        if (!isAdded) {
            return
        }
        // любая ошибка: нет интернета при первом запуске, некорректное считываение с диска и тд
    }
}

