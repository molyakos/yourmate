package com.molyakos.yourmate.presentation.view.module.phone

import com.molyakos.yourmate.domain.models.CallLogModel

interface IPhoneView {
    fun sendPhoneAction()
    fun updateCallLogList(list: List<CallLogModel>)
    fun showErrorCallLogList(e: Throwable)
}