package com.molyakos.yourmate.presentation.view.module.main

interface IMainView {
    enum class FragmentType {
        PHONE, HOME, FAVORITE
    }

    fun loadFragment(fragmentType: FragmentType)
    fun updateSMSCount(count: Int)
    fun updateCallsCount(count: Int)
}