package com.molyakos.yourmate.presentation.models

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Parcel
import android.os.Parcelable
import io.mironov.smuggler.AutoParcelable

@SuppressLint("ParcelCreator")
data class ShortCutModel(val name: String, val path: String?, val bitmap: Bitmap?): AutoParcelable
