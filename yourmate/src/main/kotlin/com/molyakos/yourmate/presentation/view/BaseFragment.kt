package com.molyakos.yourmate.presentation.view

import android.app.Activity
import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.view.View
import com.molyakos.yourmate.presentation.view.module.main.IMainView
import com.trello.rxlifecycle2.LifecycleTransformer
import com.trello.rxlifecycle2.RxLifecycle
import com.trello.rxlifecycle2.android.FragmentEvent
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

abstract class BaseFragment: Fragment(), IBaseFragment {

    private val lifecycleSubject = BehaviorSubject.create<FragmentEvent>()
    protected lateinit var hostView: IMainView

    protected abstract fun initDependencies(hostView: IMainView)
    protected abstract fun destroyDependencies()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        hostView = context as IMainView
        initDependencies(hostView)
        lifecycleSubject.onNext(FragmentEvent.ATTACH);
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        hostView = activity as IMainView
        initDependencies(hostView)
        lifecycleSubject.onNext(FragmentEvent.ATTACH);
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleSubject.onNext(FragmentEvent.CREATE)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleSubject.onNext(FragmentEvent.CREATE_VIEW)
    }

    override fun onStart() {
        super.onStart()
        lifecycleSubject.onNext(FragmentEvent.START)
    }

    override fun onResume() {
        super.onResume()
        lifecycleSubject.onNext(FragmentEvent.RESUME)
    }

    override fun onPause() {
        lifecycleSubject.onNext(FragmentEvent.PAUSE)
        super.onPause()
    }

    override fun onStop() {
        lifecycleSubject.onNext(FragmentEvent.STOP)
        super.onStop()
    }

    override fun onDestroyView() {
        lifecycleSubject.onNext(FragmentEvent.DESTROY_VIEW)
        super.onDestroyView()
    }

    override fun onDestroy() {
        lifecycleSubject.onNext(FragmentEvent.DESTROY)
        super.onDestroy()

        if (isRemoving) {
            destroyDependencies()
        }
    }

    override fun onDetach() {
        lifecycleSubject.onNext(FragmentEvent.DETACH)
        super.onDetach()
    }

    override fun <T : Any?> bindToLifecycle(): LifecycleTransformer<T> {
        return RxLifecycleAndroid.bindFragment(lifecycleSubject);
    }

    override fun <T : Any?> bindUntilEvent(event: FragmentEvent): LifecycleTransformer<T> {
        return RxLifecycle.bindUntilEvent(lifecycleSubject, event);
    }

    override fun lifecycle(): Observable<FragmentEvent> {
        return lifecycleSubject.hide()
    }
}