package com.molyakos.yourmate.presentation.view.module.report

import android.content.Intent
import android.os.Bundle
import com.molyakos.yourmate.R
import com.molyakos.yourmate.app.ReportHelper
import com.molyakos.yourmate.di.ReportModule
import com.molyakos.yourmate.presentation.view.BaseActivity

class ReportActivity : BaseActivity() {

    lateinit var reportHelper: ReportHelper

    override fun initDependencies() = ReportModule.inject(this)

    override fun destroyDependencies() = ReportModule.destroy()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sendMailIntent = Intent(Intent.ACTION_SEND_MULTIPLE)
        sendMailIntent.type = "text/plain"
        sendMailIntent.putExtra(Intent.EXTRA_EMAIL, reportHelper.emailAddress)

        val appInfo = reportHelper.getInfo()
        val eventLogPath = reportHelper.getEventLog()
        val systemLogPath = reportHelper.getSystemLog()

        sendMailIntent.putExtra(Intent.EXTRA_TEXT, appInfo + eventLogPath + systemLogPath)
        startActivity(Intent.createChooser(sendMailIntent, getString(R.string.title_send_email)))
        finish()
    }
}