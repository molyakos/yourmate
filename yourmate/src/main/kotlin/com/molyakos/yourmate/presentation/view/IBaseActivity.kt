package com.molyakos.yourmate.presentation.view

import com.trello.rxlifecycle2.LifecycleProvider
import com.trello.rxlifecycle2.android.ActivityEvent

interface IBaseActivity : LifecycleProvider<ActivityEvent> {
}