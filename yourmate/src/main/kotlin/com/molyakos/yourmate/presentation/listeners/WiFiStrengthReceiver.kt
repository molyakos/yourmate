package com.molyakos.yourmate.presentation.listeners

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.os.Build
import android.support.graphics.drawable.VectorDrawableCompat

class WiFiStrengthReceiver : BroadcastReceiver() {

    interface View {
        fun updateWifiDrawable(drawable: Drawable?)
    }

    private lateinit var connectivityManager: ConnectivityManager
    private lateinit var wifiManager: WifiManager
    private lateinit var view: View
    private var mStrength = 0

    override fun onReceive(context: Context, intent: Intent) {
        view = context as View
        wifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
        connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (wifiManager.isWifiEnabled) {
            if (wifiManager.wifiState == WifiManager.WIFI_STATE_ENABLED) {
                val listResults = wifiManager.scanResults
                for (result in listResults) {
                    if (result.BSSID == wifiManager.connectionInfo.bssid) {
                        val level = WifiManager.calculateSignalLevel(wifiManager.connectionInfo.rssi, result.level)
                        val difference = level * 100 / result.level
                        when (difference) {
                            in 100..difference -> mStrength = 4
                            in 80..100 -> mStrength = 4
                            in 60..80 -> mStrength = 3
                            in 40..60 -> mStrength = 2
                            in 20..40 -> mStrength = 1
                            in difference..20 -> mStrength = 0
                        }
                    }
                }
            }
            updateWifiDrawable(context)
        }
    }

    fun updateWifiDrawable(context: Context) {
        var wifiDisconnected = true
        var wifiInfo: NetworkInfo? = null // may be null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            wifiInfo = connectivityManager.getNetworkInfo(connectivityManager.activeNetwork)
        } else {
            wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        }

        if (wifiInfo == null || !wifiInfo.isConnected) {
            wifiDisconnected = false
        }

        if (wifiDisconnected) {
            val icon = StringBuilder().append("ic_signal_wifi_").append(mStrength).append("_bar_white_24dp").toString()
            val id = context.resources.getIdentifier(icon, "drawable", context.packageName)
            val drawable = VectorDrawableCompat.create(context.resources, id, null)!!
            view.updateWifiDrawable(drawable)
        }
    }
}