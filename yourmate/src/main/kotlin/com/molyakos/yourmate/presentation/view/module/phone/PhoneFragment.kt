package com.molyakos.yourmate.presentation.view.module.phone

import android.app.AlertDialog
import android.content.Intent
import android.database.ContentObserver
import android.net.Uri
import android.os.Bundle
import android.provider.CallLog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.EditText
import android.widget.Toast
import com.molyakos.yourmate.R
import com.molyakos.yourmate.di.PhoneModule
import com.molyakos.yourmate.domain.models.CallLogModel
import com.molyakos.yourmate.presentation.adapter.CallLogAdapter
import com.molyakos.yourmate.presentation.presenter.module.phone.IPhonePresenter
import com.molyakos.yourmate.presentation.view.BaseFragment
import com.molyakos.yourmate.presentation.view.module.main.IMainView
import com.molyakos.yourmate.presentation.view.module.phone.views.DialpadKeyButton
import com.molyakos.yourmate.presentation.view.module.phone.views.DialpadLayout
import java.text.SimpleDateFormat
import java.util.*

class PhoneFragment : BaseFragment(), IPhoneView, DialpadKeyButton.OnPressedListener {

    lateinit var presenter: IPhonePresenter
    private lateinit var recyclerView: RecyclerView
    private lateinit var dialpadLayout: DialpadLayout
    private lateinit var digitsEditText: EditText
    private lateinit var deleteView: View
    private lateinit var callLogAdapter: CallLogAdapter
    private var dialpadHidden = false

    private val onClickListener = View.OnClickListener { v ->
        when (v.id) {
            R.id.delete_button -> keyPressed(KeyEvent.KEYCODE_DEL)
            R.id.digits -> {
                if (digitsEditText.length() != 0) {
                    digitsEditText.isCursorVisible = true
                }
            }
        }
    }

    private val textChangedListener = object : TextWatcher {
        override fun afterTextChanged(s: Editable) {
            digitsEditText.isCursorVisible = false
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }

    private val onLongClickListener = View.OnLongClickListener { v ->
        when (v.id) {
            R.id.delete_button -> {
                clearDigits()
                true
            }
            else -> false
        }
    }

    private val calLogContentObserver = object : ContentObserver(null) {
        override fun onChange(selfChange: Boolean) {
            this.onChange(selfChange, null)
        }

        override fun onChange(selfChange: Boolean, uri: Uri?) {
            presenter.updateCallLog()
        }
    }

    private val callLogItemOnClickListener = object : CallLogAdapter.OnClickListener {
        override fun onClick(v: View, item: CallLogModel) {
            when (v.id) {
                R.id.layout -> {
                    sendDial(item.number)
                }
            }
        }
    }

    private val callLogItemOnLongClickListener = object : CallLogAdapter.OnLongClickListener {
        override fun onClick(v: View, item: CallLogModel) {
            when (v.id) {
                R.id.layout -> {
                    val calendar = Calendar.getInstance()
                    calendar.timeInMillis = item.date.toLong()
                    val format = SimpleDateFormat("EEEE, d MMMM, yyyy, HH:mm", Locale.getDefault())

                    AlertDialog.Builder(this@PhoneFragment.activity)
                            .setTitle(R.string.title_info_about_call)
                            .setMessage(String.format(getString(R.string.message_info_about_call),
                                    item.name,
                                    item.number,
                                    item.duration,
                                    format.format(calendar.time),
                                    item.type))
                            .setPositiveButton("OK", null)
                            .show()
                }
            }
        }
    }

    override fun initDependencies(hostView: IMainView) = PhoneModule.inject(this)

    override fun destroyDependencies() = PhoneModule.destroy()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.phone_fragment, container, false)

        dialpadLayout = view.findViewById(R.id.dialpad_view) as DialpadLayout

        dialpadLayout.setCanDigitsBeEdited(true)
        digitsEditText = dialpadLayout.getDigits()
        digitsEditText.setOnClickListener(onClickListener)
        digitsEditText.addTextChangedListener(textChangedListener)
        deleteView = dialpadLayout.getDeleteButton()
        deleteView.setOnClickListener(onClickListener)
        deleteView.setOnLongClickListener(onLongClickListener)
        configureKeypadListeners(view)

        recyclerView = view.findViewById(R.id.call_log) as RecyclerView
        callLogAdapter = CallLogAdapter(activity, emptyList(), callLogItemOnClickListener, callLogItemOnLongClickListener)

        recyclerView.adapter = callLogAdapter
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 && !dialpadHidden) {
                    dialpadHidden = true
                    dialpadLayout.animate().translationY(dialpadLayout.height.toFloat())
                }
            }
        })

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        savedInstanceState?.let { digitsEditText.setText(it.getString("digits")) }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        activity.contentResolver.registerContentObserver(CallLog.Calls.CONTENT_URI, true, calLogContentObserver)

        presenter.bind(this)
        presenter.prepareToneGenerator()
    }

    override fun onResume() {
        super.onResume()

        presenter.updateCallLog()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("digits", digitsEditText.text.toString())
        super.onSaveInstanceState(outState)
    }

    override fun onStop() {
        super.onStop()

        activity.contentResolver.unregisterContentObserver(calLogContentObserver)

        presenter.stopToneGenerator()
        presenter.unbind()
    }

    private fun clearDigits() {
        digitsEditText.text.clear()
    }

    private fun configureKeypadListeners(fragmentView: View) {
        val buttonIds = intArrayOf(R.id.one, R.id.two, R.id.three, R.id.four, R.id.five, R.id.six, R.id.seven, R.id.eight, R.id.nine, R.id.star, R.id.zero, R.id.pound)

        for (index in buttonIds.indices) {
            val dialpadKeyButton = fragmentView.findViewById(buttonIds[index]) as DialpadKeyButton
            dialpadKeyButton.setOnPressedListener(this)
        }
    }

    override fun onPressed(view: View, pressed: Boolean) {
        if (pressed) {
            when (view.id) {
                R.id.one -> keyPressed(KeyEvent.KEYCODE_1)
                R.id.two -> keyPressed(KeyEvent.KEYCODE_2)
                R.id.three -> keyPressed(KeyEvent.KEYCODE_3)
                R.id.four -> keyPressed(KeyEvent.KEYCODE_4)
                R.id.five -> keyPressed(KeyEvent.KEYCODE_5)
                R.id.six -> keyPressed(KeyEvent.KEYCODE_6)
                R.id.seven -> keyPressed(KeyEvent.KEYCODE_7)
                R.id.eight -> keyPressed(KeyEvent.KEYCODE_8)
                R.id.nine -> keyPressed(KeyEvent.KEYCODE_9)
                R.id.zero -> keyPressed(KeyEvent.KEYCODE_0)
                R.id.pound -> keyPressed(KeyEvent.KEYCODE_POUND)
                R.id.star -> keyPressed(KeyEvent.KEYCODE_STAR)
            }
        }
    }

    private fun keyPressed(keyCode: Int) {
        if (view == null || dialpadLayout.translationY != 0f) {
            return
        }

        presenter.keyPressed(keyCode)

        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
        val event = KeyEvent(KeyEvent.ACTION_DOWN, keyCode)
        digitsEditText.onKeyDown(keyCode, event)

        if (digitsEditText.length() == digitsEditText.selectionStart && digitsEditText.length() == digitsEditText.selectionEnd) {
            digitsEditText.isCursorVisible = false
        }
    }

    override fun sendPhoneAction() {
        if (dialpadHidden) {
            dialpadHidden = false
            dialpadLayout.animate().translationY(0f)
        } else {
            callCurrentPhoneNumber()
        }
    }

    private fun callCurrentPhoneNumber() {
        val phoneNumber = digitsEditText.text.toString()
        if (phoneNumber.isNotEmpty()) {
            sendDial(phoneNumber)
        } else {
            showPhoneValidationToast()
        }
        clearDigits()
    }

    private fun sendDial(phoneNumber: String) {
        val callIntent = Intent(Intent.ACTION_CALL)
        callIntent.data = Uri.parse("tel:" + Uri.encode(phoneNumber))
        startActivity(callIntent)
    }

    private fun showPhoneValidationToast() {
        Toast.makeText(activity, getString(R.string.validation_enter_phone_number), Toast.LENGTH_SHORT).show()
    }

    override fun updateCallLogList(list: List<CallLogModel>) = callLogAdapter.run {
        updateList(list)
        notifyDataSetChanged()
    }

    override fun showErrorCallLogList(e: Throwable) {
        Toast.makeText(activity, R.string.error_occurred, Toast.LENGTH_SHORT).show()
    }
}