package com.molyakos.yourmate.presentation.presenter.module.phone

import com.molyakos.yourmate.domain.controllers.IToneController
import com.molyakos.yourmate.domain.interactors.ICallLogInteractor
import com.molyakos.yourmate.presentation.view.IBaseFragment
import com.molyakos.yourmate.presentation.view.module.phone.IPhoneView
import com.trello.rxlifecycle2.kotlin.bindToLifecycle

class PhonePresenter(val toneController: IToneController,
                     val interactor: ICallLogInteractor) : IPhonePresenter {

    private var view: IPhoneView? = null
    private var androidView: IBaseFragment? = null

    override fun bind(phoneView: IPhoneView) {
        view = phoneView
        androidView = phoneView as IBaseFragment
    }

    override fun unbind() {
        view = null
    }

    override fun prepareToneGenerator() {
        toneController.prepare()
    }

    override fun keyPressed(keyCode: Int) {
        toneController.keyPressed(keyCode)
    }

    override fun stopToneGenerator() {
        toneController.stop()
    }

    override fun updateCallLog() {
        interactor.updateCallLog()
                .bindToLifecycle(androidView!!)
                .subscribe({
                    view?.updateCallLogList(it)
                }, {
                    view?.showErrorCallLogList(it)
                })
    }
}