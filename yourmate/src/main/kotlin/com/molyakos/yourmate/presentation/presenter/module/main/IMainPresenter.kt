package com.molyakos.yourmate.presentation.presenter.module.main

import com.molyakos.yourmate.presentation.view.module.main.IMainView

interface IMainPresenter {
    fun bind(iMainView: IMainView)
    fun unbind()
}