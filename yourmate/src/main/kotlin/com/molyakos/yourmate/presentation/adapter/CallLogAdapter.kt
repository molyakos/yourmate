package com.molyakos.yourmate.presentation.adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.molyakos.yourmate.R
import com.molyakos.yourmate.domain.models.CallLogModel

class CallLogAdapter(private val context: Context,
                     private var items: List<CallLogModel>,
                     private val onClickListener: OnClickListener,
                     private val onLongClickListener: OnLongClickListener) : RecyclerView.Adapter<CallLogAdapter.ViewHolder>() {

    interface OnClickListener {
        fun onClick(v: View, item: CallLogModel)
    }

    interface OnLongClickListener {
        fun onClick(v: View, item: CallLogModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_call_log, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.nameTextView.text = items[position].name

        val missing = items[position].type == context.getString(R.string.missed_call)
        if (missing) {
            holder.nameTextView.setTextColor(Color.RED)
        } else {
            holder.nameTextView.setTextColor(Color.BLACK)
        }

        holder.cardView.setOnClickListener {
            onClickListener.onClick(holder.cardView, items[holder.adapterPosition])
        }

        holder.cardView.setOnLongClickListener {
            onLongClickListener.onClick(holder.cardView, items[holder.adapterPosition])
            true
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameTextView: TextView = view.findViewById(R.id.item_name) as TextView
        val cardView: RelativeLayout = view.findViewById(R.id.layout) as RelativeLayout
    }

    fun updateList(list: List<CallLogModel>) {
        items = list
    }
}