package com.molyakos.yourmate.app

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import com.molyakos.yourmate.R

class PermissionAlertDialog(val context: Context, titleResId: Int, val messageResId: Int) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(context)
    private var dialog: AlertDialog? = null

    init {
        builder.setCancelable(false)
        builder.setTitle(titleResId)
        builder.setMessage(messageResId)
        builder.setNegativeButton(R.string.deny) { dialog, which -> dialog.dismiss() }
    }

    fun setPositiveButton(textId: Int, listener: DialogInterface.OnClickListener?): PermissionAlertDialog {
        return apply { builder.setPositiveButton(textId, listener) }
    }

    fun setNegativeButton(listener: DialogInterface.OnClickListener?): PermissionAlertDialog {
        return apply { builder.setNegativeButton("", listener) }
    }

    fun setNegativeButton(textId: Int, listener: DialogInterface.OnClickListener?): PermissionAlertDialog {
        return apply { builder.setNegativeButton(textId, listener) }
    }

    fun setOnDismissListener(onDismissListener: DialogInterface.OnDismissListener?): PermissionAlertDialog {
        return apply { builder.setOnDismissListener(onDismissListener) }
    }

    fun show() {
        dialog = builder.create()
        dialog!!.show()
    }

    fun dismiss() {
        dialog?.dismiss()
    }
}
