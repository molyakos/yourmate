package com.molyakos.yourmate.app

import android.os.Build
import java.io.*

class ApplicationInfo(
        val packageName: String,
        val versionName: String,
        val versionCode: Int)

interface IReportHelper {
    fun getEventLog(): String
    fun getSystemLog(): String
    fun getInfo(): String
}

class ReportHelper(private val mApplicationInfo: ApplicationInfo): IReportHelper {
    private val EVENT_LOG_CMD = arrayOf("logcat", "-d", "-b", "events", "-v", "time")
    private val SYSTEM_LOG_CMD = arrayOf("logcat", "-d", "-v", "time")
    val emailAddress = "molyakos@gmail.com"

    override fun getEventLog(): String {
        return captureCommand(EVENT_LOG_CMD)
    }

    override fun getSystemLog(): String {
        return captureCommand(SYSTEM_LOG_CMD)
    }

    override fun getInfo(): String {
        val writer = StringBuilder()
        writer.append("\n\n\n\n\n\n\n\n")
        writer.append("Do not delete\n")
        writer.append("=============\n")
        writer.append("Package: ").append(mApplicationInfo.packageName).append('\n')
        writer.append("Version Name: ").append(mApplicationInfo.versionName).append('\n')
        writer.append("Version Code: ").append(mApplicationInfo.versionCode.toChar()).append('\n')
        writer.append("Brand: ").append(Build.MODEL).append('\n')
        writer.append("Version OS: ").append(Build.VERSION.RELEASE).append('\n')
        writer.append("Model: ").append(Build.DEVICE).append('\n')
        writer.append("Product: ").append(Build.PRODUCT).append('\n')
        writer.append("Type: ").append(Build.TYPE).append("\n\n")
        return writer.toString()
    }

    private fun captureCommand(command: Array<String>): String {
        val process = ProcessBuilder(*command).redirectErrorStream(true).start()
        val inputStream = process!!.inputStream
        val writer = StringBuilder()
        val logText = inputStream.bufferedReader().use(BufferedReader::readText)
        writer.append(logText)
        return writer.toString()
    }
}