package com.molyakos.yourmate.app

import android.content.Context
import com.google.gson.Gson
import com.molyakos.yourmate.data.entity.WeatherEntity
import io.reactivex.Observable
import java.io.FileNotFoundException

interface IDiskIO {
    fun readWeather(fileName: String): Observable<String>
    fun writeWeather(fileName: String, model: WeatherEntity): Observable<Any>
}

class DiskIO(private val context: Context) : IDiskIO {
    override fun readWeather(fileName: String): Observable<String> {
        return Observable.create { emitter ->
            val inputStream = context.openFileInput(fileName)
            val contentString: String = inputStream.bufferedReader().use { it.readText() }
            emitter.onNext(contentString)
            emitter.onComplete()
        }
    }

    override fun writeWeather(fileName: String, model: WeatherEntity): Observable<Any> {
        return Observable.fromCallable {
            val outputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE)
            outputStream.write(Gson().toJson(model).toByteArray())
        }
    }
}