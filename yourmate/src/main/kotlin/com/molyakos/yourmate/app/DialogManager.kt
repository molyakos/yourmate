package com.molyakos.yourmate.app

import android.content.DialogInterface

interface IDialogManager {
    fun showAll()
    fun insert(dialog: PermissionAlertDialog)
    fun add(dialog: PermissionAlertDialog)
}

class DialogManager : IDialogManager {

    private val dialogList = ArrayList<PermissionAlertDialog>()
    private val newDialogList = ArrayList<PermissionAlertDialog>()

    override fun add(dialog: PermissionAlertDialog) {
        dialogList.add(dialog)
    }

    override fun insert(dialog: PermissionAlertDialog) {
        var exists = false
        for (it in dialogList) {
            if (it.messageResId == dialog.messageResId) {
                exists = true
            }
            it.setOnDismissListener(null)
            it.dismiss()
        }

        if (!exists) {
            dialogList.add(0, dialog)
        }

        newDialogList.clear()
        newDialogList.addAll(dialogList)

        for (index in newDialogList.indices) {
            val nextIndex = index + 1
            newDialogList[index].setOnDismissListener(DialogInterface.OnDismissListener {
                if (nextIndex > newDialogList.size - 1) {
                    return@OnDismissListener
                } else {
                    newDialogList[nextIndex].show()
                }
            })
        }

        newDialogList[0].show()
    }

    override fun showAll() {
        if (dialogList.isEmpty()) return

        for (index in dialogList.indices) {
            val nextIndex = index + 1
            dialogList[index].setOnDismissListener(DialogInterface.OnDismissListener {
                if (newDialogList.size == 0) {
                    if (nextIndex > dialogList.size - 1) {
                        return@OnDismissListener
                    } else {
                        dialogList[nextIndex].show()
                    }
                }
            })
        }

        dialogList[0].show()
    }
}