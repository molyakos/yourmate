package com.molyakos.yourmate.app

import java.text.SimpleDateFormat
import java.util.*

interface IDateFormatter {
    fun toMillisDate(date: String, format: String): Long
    fun toStringDate(millis: Long, format: String): String
}

class DateFormatter : IDateFormatter {
    override fun toMillisDate(date: String, format: String): Long {
        return SimpleDateFormat(format, Locale.getDefault()).parse(date).time
    }

    override fun toStringDate(millis: Long, format: String): String {
        return SimpleDateFormat(format, Locale.getDefault()).format(Date(millis*1000))
    }
}