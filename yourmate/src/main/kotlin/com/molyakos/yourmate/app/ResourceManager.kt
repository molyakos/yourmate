package com.molyakos.yourmate.app

import android.content.Context

interface IResourceManager {
    fun getString(resId: Int): String
}

class ResourceManager(private val mContext: Context): IResourceManager {
    override fun getString(resId: Int): String {
        return mContext.getString(resId)
    }
}