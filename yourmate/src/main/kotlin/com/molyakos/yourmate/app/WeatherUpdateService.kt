package com.molyakos.yourmate.app

import android.app.Service
import android.app.job.JobParameters
import android.app.job.JobService
import com.molyakos.yourmate.di.WeatherUpdateServiceModule
import com.molyakos.yourmate.domain.interactors.IWeatherInteractor
import android.content.Intent
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import com.molyakos.yourmate.domain.interactors.ISettingsInteractor
import com.molyakos.yourmate.presentation.view.module.home.HomeFragment

class WeatherUpdateService : JobService() {

    lateinit var weatherInteractor: IWeatherInteractor
    lateinit var settingsInteractor: ISettingsInteractor
    private var activityMessenger: Messenger? = null

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        WeatherUpdateServiceModule.inject(this)
        activityMessenger = intent.getParcelableExtra(HomeFragment.MESSENGER_INTENT_KEY)
        return Service.START_NOT_STICKY
    }

    override fun onStartJob(params: JobParameters?): Boolean {
        val cityName = settingsInteractor.fetchCurrentCityName()
        val units = settingsInteractor.fetchUnits()
        weatherInteractor.updateWeather(cityName, units)
                .subscribe({
                    val message = Message.obtain()
                    message.what = 1
                    message.obj = it
                    try {
                        activityMessenger?.send(message)
                    } catch (ignored: RemoteException) {
                    }
                    jobFinished(params, true)
                }, {
                    jobFinished(params, true)
                })
        return true
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        return true
    }

    override fun onDestroy() {
        WeatherUpdateServiceModule.destroy()
        super.onDestroy()
    }
}