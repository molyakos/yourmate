package com.molyakos.yourmate.data.net

import okhttp3.OkHttpClient
import okhttp3.Request
import java.net.URL
import java.util.concurrent.Callable

class ApiConnection constructor(private val url: String) {

    fun connectToApi(): String? {
        val request = Request.Builder()
                .url(url)
                .get()
                .build()

        try {
            return OkHttpClient().newCall(request).execute().body()!!.string()
        } catch (ignored: Exception) {
            return null
        }
    }
}