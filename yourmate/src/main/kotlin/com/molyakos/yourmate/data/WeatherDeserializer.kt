package com.molyakos.yourmate.data

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.molyakos.yourmate.data.entity.WeatherEntity
import java.lang.reflect.Type

class WeatherDeserializer(private val tempUnits: String) : JsonDeserializer<WeatherEntity> {

    override fun deserialize(json: JsonElement, type: Type, jdc: JsonDeserializationContext): WeatherEntity {
        val jsonObject = json.asJsonObject
        val cityName = jsonObject.getAsJsonPrimitive("name").asString
        val mainObject = jsonObject.getAsJsonObject("main").asJsonObject
        val temp = mainObject.getAsJsonPrimitive("temp").asString.toDouble().toInt()
        val weatherArray = jsonObject.getAsJsonArray("weather").asJsonArray
        val iconObject = weatherArray.get(0).asJsonObject
        val icon = iconObject.getAsJsonPrimitive("icon").asString
        val sysObject = jsonObject.getAsJsonObject("sys").asJsonObject
        val sunrise = sysObject.getAsJsonPrimitive("sunrise").asLong
        val sunset = sysObject.getAsJsonPrimitive("sunset").asLong
        return WeatherEntity(cityName, sunrise, sunset, temp, icon, tempUnits)
    }
}