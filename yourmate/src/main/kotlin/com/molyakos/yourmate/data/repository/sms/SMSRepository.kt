package com.molyakos.yourmate.data.repository.sms

import com.molyakos.yourmate.data.repository.sms.datastore.ISMSDiskStore
import com.molyakos.yourmate.domain.repository.ISMSRepository
import io.reactivex.Observable

class SMSRepository(private val diskStore: ISMSDiskStore): ISMSRepository {
    override fun fetchMissedSMSCount(): Observable<Int> {
        return diskStore.fetchMissedSMSCount()
    }
}