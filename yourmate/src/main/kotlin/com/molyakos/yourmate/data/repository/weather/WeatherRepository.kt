package com.molyakos.yourmate.data.repository.weather

import com.google.gson.*
import com.molyakos.yourmate.data.entity.WeatherEntity
import com.molyakos.yourmate.data.entity.mapper.WeatherEntityDataMapper
import com.molyakos.yourmate.data.repository.weather.datastore.IWeatherCloudStore
import com.molyakos.yourmate.data.repository.weather.datastore.IWeatherDiskStore
import com.molyakos.yourmate.domain.models.WeatherModel
import com.molyakos.yourmate.domain.repository.IWeatherRepository
import io.reactivex.Observable

class WeatherRepository(private val weatherMapper: WeatherEntityDataMapper,
                        private val diskStore: IWeatherDiskStore,
                        private val cloudStore: IWeatherCloudStore,
                        private val gsonWithCloudDeserializer: Gson,
                        private val gson: Gson) : IWeatherRepository {

    override fun writeWeatherToDiskStore(weatherModel: WeatherEntity) {
        diskStore.writeWeather(weatherModel).subscribe()
    }

    override fun fetchWeatherFromDiskStore(): Observable<WeatherModel> {
        return diskStore.fetchWeather()
                .map { gson.fromJson<WeatherEntity>(it, WeatherEntity::class.java) }
                .map { weatherMapper.transform(it, it.tempUnits) }
                .onErrorResumeNext(Observable.empty())
    }

    override fun fetchWeatherFromCloudStore(cityName: String, units: String): Observable<WeatherModel> {
        return cloudStore.fetchWeather(cityName, units)
                .map { gsonWithCloudDeserializer.fromJson<WeatherEntity>(it, WeatherEntity::class.java) }
                .doOnNext { writeWeatherToDiskStore(it) }
                .map { weatherMapper.transform(it, units) }
                .onErrorResumeNext(Observable.empty())
    }
}