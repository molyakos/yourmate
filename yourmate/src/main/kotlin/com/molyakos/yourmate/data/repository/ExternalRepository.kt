package com.molyakos.yourmate.data.repository

import android.content.ContentResolver
import android.database.Cursor
import android.provider.CallLog
import android.provider.Telephony
import com.molyakos.yourmate.data.entity.CallLogEntity
import io.reactivex.Observable

interface IExternalRepository {
    fun fetchMissedSMSCount(): Observable<Int>
    fun fetchMissedCallsCount(): Observable<Int>
    fun fetchCallLogList(): Observable<List<CallLogEntity>>
}

class ExternalRepository(val contentResolver: ContentResolver) : IExternalRepository {
    override fun fetchMissedSMSCount(): Observable<Int> {
        return Observable.create { emitter ->
            var cursor: Cursor? = null
            try {
                val selection = "${Telephony.Sms.READ} = 0"
                cursor = contentResolver.query(Telephony.Sms.CONTENT_URI, null, selection, null, null)
                emitter.onNext(cursor.count)
                emitter.onComplete()
            } catch (e: Exception) {
                emitter.onError(e)
            } finally {
                cursor?.close()
            }
        }
    }

    override fun fetchMissedCallsCount(): Observable<Int> {
        return Observable.create { emitter ->
            val where = "${CallLog.Calls.TYPE} = ${CallLog.Calls.MISSED_TYPE}  AND ${CallLog.Calls.NEW} = 1"
            var cursor: Cursor? = null
            try {
                cursor = contentResolver.query(CallLog.Calls.CONTENT_URI, null, where, null, null)
                emitter.onNext(cursor.count)
                emitter.onComplete()
            } catch (ignored: Exception) {
            } finally {
                cursor?.close()
            }
        }
    }

    override fun fetchCallLogList(): Observable<List<CallLogEntity>> {
        return Observable.create { emitter ->
            val list: List<CallLogEntity>
            var callLogCursor: Cursor? = null
            try {
                val oreder = "${CallLog.Calls.DATE} DESC"
                callLogCursor = contentResolver.query(CallLog.Calls.CONTENT_URI, null, null,
                        null, oreder)
                list = ArrayList<CallLogEntity>(callLogCursor.count)
                if (callLogCursor.moveToFirst()) {
                    do {
                        if (callLogCursor.getPosition() < 30) {
                            val numberColumn = callLogCursor.getColumnIndex(CallLog.Calls.NUMBER)
                            val nameColumn = callLogCursor.getColumnIndex(CallLog.Calls.CACHED_NAME)
                            val typeColumn = callLogCursor.getColumnIndex(CallLog.Calls.TYPE)
                            val dateColumn = callLogCursor.getColumnIndex(CallLog.Calls.DATE)
                            val durationColumn = callLogCursor.getColumnIndex(CallLog.Calls.DURATION)

                            val number = callLogCursor.getString(numberColumn)
                            var name = callLogCursor.getString(nameColumn)
                            if (name == null) name = number // some dialer's phones may not have CACHED_NAME, but I don'toStringDate care
                            val type = callLogCursor.getInt(typeColumn)
                            val date = callLogCursor.getString(dateColumn)
                            val duration = callLogCursor.getString(durationColumn)

                            val call = CallLogEntity(name, number, type, date, duration)
                            list.add(call)
                        } else {
                            break
                        }
                    } while (callLogCursor.moveToNext())
                }
                emitter.onNext(list)
                emitter.onComplete()
            } catch (e: Exception) {
                emitter.onError(e)
            } finally {
                callLogCursor?.close()
            }
        }
    }
}