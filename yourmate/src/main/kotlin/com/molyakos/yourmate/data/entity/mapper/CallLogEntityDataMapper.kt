package com.molyakos.yourmate.data.entity.mapper

import com.molyakos.yourmate.R
import com.molyakos.yourmate.app.IResourceManager
import com.molyakos.yourmate.data.entity.CallLogEntity
import com.molyakos.yourmate.domain.models.CallLogModel

open class CallLogEntityDataMapper(private val resourceManager: IResourceManager) {
    fun transform(calLogList: List<CallLogEntity>): List<CallLogModel> {
        val list = ArrayList<CallLogModel>(calLogList.size)
        val numberUnknown = resourceManager.getString(R.string.number_unknown)
        val missedCall = resourceManager.getString(R.string.missed_call)
        val incomingCall = resourceManager.getString(R.string.incoming_call)
        val outgoingCall = resourceManager.getString(R.string.outgoing_call)
        calLogList.forEach {
            val model = CallLogModel(it.name)
            var number = it.number
            val type: String
            val date = it.date
            val duration = it.duration

            if (number == "-1") {
                number = numberUnknown
            }

            when (it.type) {
                1 -> type = incomingCall
                2 -> type = outgoingCall
                else -> type = missedCall
            }

            model.number = number
            model.type = type
            model.date = date
            model.duration = duration
            list.add(model)
        }
        return list
    }
}