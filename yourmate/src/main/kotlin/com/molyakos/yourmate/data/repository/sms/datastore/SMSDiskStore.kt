package com.molyakos.yourmate.data.repository.sms.datastore

import com.molyakos.yourmate.data.repository.IExternalRepository
import io.reactivex.Observable

interface ISMSDiskStore {
    fun fetchMissedSMSCount(): Observable<Int>
}

class SMSDiskStore(private val contentResolverWrapper: IExternalRepository) : ISMSDiskStore {
    override fun fetchMissedSMSCount(): Observable<Int> {
        return Observable.create { emitter ->
            contentResolverWrapper.fetchMissedSMSCount()
        }
    }
}