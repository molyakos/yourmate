package com.molyakos.yourmate.data.net

import io.reactivex.Observable

class RestApi: IRestApi {

    private val TOKEN = "ecc7f93f689bbef0756c4048cc24be44"

    override fun getWeather(cityName: String, units: String): Observable<String> {
        val url = "http://api.openweathermap.org/data/2.5/weather?q=$cityName&units=$units&appid=$TOKEN"
        return Observable.create { emmiter ->
            val stringJson = ApiConnection(url).connectToApi()
            if (stringJson != null) {
                emmiter.onNext(stringJson)
                emmiter.onComplete()
            } else {
                emmiter.onError(Exception("Network Connection Exception"))
            }
        }
    }
}