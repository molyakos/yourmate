package com.molyakos.yourmate.data.repository.settings

import com.molyakos.yourmate.data.repository.settings.datastore.SettingsDiskStore
import com.molyakos.yourmate.domain.repository.ISettingsRepository

class SettingsRepository(private val mDiskSettingsStore: SettingsDiskStore): ISettingsRepository {
    override fun fetchUnits(): String {
        return mDiskSettingsStore.fetchUnits()
    }
    override fun fetchCurrentCityName(): String {
        return mDiskSettingsStore.fetchCurrentCityName()
    }
}