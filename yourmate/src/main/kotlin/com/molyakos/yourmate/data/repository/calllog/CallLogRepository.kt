package com.molyakos.yourmate.data.repository.calllog

import com.molyakos.yourmate.data.entity.mapper.CallLogEntityDataMapper
import com.molyakos.yourmate.domain.models.CallLogModel
import com.molyakos.yourmate.data.repository.calllog.datastore.ICallLogDiskStore
import com.molyakos.yourmate.domain.repository.ICallLogRepository
import io.reactivex.Observable

class CallLogRepository(private val callLogEntityDataMapper: CallLogEntityDataMapper,
                        private val diskStore: ICallLogDiskStore) : ICallLogRepository {

    override fun fetchMissedCallsCount(): Observable<Int> {
        return diskStore.fetchMissedCallsCount()
    }

    override fun fetchCallLogList(): Observable<List<CallLogModel>> {
        return diskStore.fetchCallLogList().map { callLogEntityDataMapper.transform(it) }
    }
}