package com.molyakos.yourmate.data.repository.calllog.datastore

import com.molyakos.yourmate.data.repository.IExternalRepository
import com.molyakos.yourmate.data.entity.CallLogEntity
import io.reactivex.Observable

interface ICallLogDiskStore {
    fun fetchMissedCallsCount(): Observable<Int>
    fun fetchCallLogList(): Observable<List<CallLogEntity>>
}

class CallLogDiskStore(private val contentResolverWrapper: IExternalRepository) : ICallLogDiskStore {

    override fun fetchMissedCallsCount(): Observable<Int> {
        return contentResolverWrapper.fetchMissedCallsCount()
    }

    override fun fetchCallLogList(): Observable<List<CallLogEntity>> {
        return contentResolverWrapper.fetchCallLogList()
    }
}