package com.molyakos.yourmate.data.repository.settings.datastore

import android.content.SharedPreferences

class SettingsDiskStore(private val sharedPreference: SharedPreferences) {

    private val cityNameKey = "city_name"
    private val defaultCityName = "moscow"

    private val unitsKey = "temp_units"
    private val defaultUnits = "metric"

    fun fetchCurrentCityName(): String {
        return sharedPreference.getString(cityNameKey, defaultCityName)
    }

    fun fetchUnits(): String {
        return sharedPreference.getString(unitsKey, defaultUnits)
    }
}