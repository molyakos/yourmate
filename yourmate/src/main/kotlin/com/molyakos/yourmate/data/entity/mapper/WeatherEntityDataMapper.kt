package com.molyakos.yourmate.data.entity.mapper

import com.molyakos.yourmate.data.entity.WeatherEntity
import com.molyakos.yourmate.domain.models.WeatherModel
import com.molyakos.yourmate.app.IDateFormatter

open class WeatherEntityDataMapper(private val dateFormat: IDateFormatter,
                                   private val metric: String) {

    fun transform(weatherEntity: WeatherEntity, units: String): WeatherModel {
        val cityName = weatherEntity.cityName
        val sunrise = dateFormat.toStringDate(weatherEntity.sunrise, "HH:mm")
        val sunset = dateFormat.toStringDate(weatherEntity.sunset, "HH:mm")
        val temp = weatherEntity.temp.toString()
        val icon = weatherEntity.icon
        val tempUnits: String

        if (units == metric) {
            tempUnits = "C"
        } else {
            tempUnits = "F"
        }
        return WeatherModel(cityName, sunrise, sunset, temp, icon, tempUnits)
    }
}