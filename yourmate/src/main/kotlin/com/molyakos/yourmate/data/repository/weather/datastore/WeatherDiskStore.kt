package com.molyakos.yourmate.data.repository.weather.datastore

import com.molyakos.yourmate.app.IDiskIO
import com.molyakos.yourmate.data.entity.WeatherEntity
import com.molyakos.yourmate.domain.models.WeatherModel
import io.reactivex.Observable

interface IWeatherDiskStore {
    fun fetchWeather(): Observable<String>
    fun writeWeather(model: WeatherEntity): Observable<Any>
}

class WeatherDiskStore(private val diskWrapper: IDiskIO): IWeatherDiskStore {

    private val weatherFileName = "weather"

    override fun fetchWeather(): Observable<String> {
        return diskWrapper.readWeather(weatherFileName)
    }

    override fun writeWeather(model: WeatherEntity): Observable<Any> {
        return diskWrapper.writeWeather(weatherFileName, model)
    }
}