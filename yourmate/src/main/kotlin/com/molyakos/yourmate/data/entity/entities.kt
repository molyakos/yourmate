package com.molyakos.yourmate.data.entity

import com.google.gson.annotations.SerializedName

data class CallLogEntity(var name: String, var number: String, var type: Int, var date: String, var duration: String)

data class WeatherEntity constructor(
        @SerializedName("name")
        val cityName: String,
        @SerializedName("sunrise")
        val sunrise: Long,
        @SerializedName("sunset")
        val sunset: Long,
        @SerializedName("temp")
        val temp: Int,
        @SerializedName("icon")
        val icon: String,
        @SerializedName("units")
        val tempUnits: String)