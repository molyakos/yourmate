package com.molyakos.yourmate.data.cache

import android.graphics.Bitmap
import android.util.LruCache

object ImageCache {

    private var memoryCache: LruCache<Int, Bitmap>

    init {
        val maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()
        val cacheSize = maxMemory / 8
        memoryCache = object : LruCache<Int, Bitmap>(cacheSize) {
            override fun sizeOf(key: Int?, bitmap: Bitmap): Int {
                return bitmap.byteCount / 1024
            }
        }
    }

    fun addBitmapToMemoryCache(key: Int, bitmap: Bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            memoryCache.put(key, bitmap)
        }
    }

    fun getBitmapFromMemCache(key: Int): Bitmap? {
        return memoryCache.get(key)
    }

    fun clearBitmapFromMemCache() {
        if (memoryCache.size() != 0)
            for (position in 0..memoryCache.size() - 1) {
                memoryCache.remove(position)
            }
    }
}