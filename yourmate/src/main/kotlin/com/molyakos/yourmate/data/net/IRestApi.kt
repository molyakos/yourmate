package com.molyakos.yourmate.data.net

import io.reactivex.Observable

interface IRestApi {
    fun getWeather(cityName: String, units: String): Observable<String>
}