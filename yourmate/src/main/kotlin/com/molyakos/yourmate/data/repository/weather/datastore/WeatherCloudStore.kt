package com.molyakos.yourmate.data.repository.weather.datastore

import com.molyakos.yourmate.data.net.IRestApi
import io.reactivex.Observable

interface IWeatherCloudStore {
    fun fetchWeather(cityName: String, units: String): Observable<String>
}

class WeatherCloudStore(private val restApi: IRestApi): IWeatherCloudStore {
    override fun fetchWeather(cityName: String, units: String): Observable<String> {
        return restApi.getWeather(cityName, units)
    }
}