package com.molyakos.yourmate.data

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.molyakos.yourmate.data.entity.WeatherEntity
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.lang.reflect.Type

@RunWith(MockitoJUnitRunner::class)
class WeatherDeserializerTest {

    val weatherApiString = "{\"coord\":{\"lon\":73.4,\"lat\":55},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"base\":\"stations\",\"main\":{\"temp\":23,\"pressure\":1012,\"humidity\":49,\"temp_min\":23,\"temp_max\":23},\"visibility\":10000,\"wind\":{\"speed\":3,\"deg\":330},\"clouds\":{\"all\":0},\"dt\":1504276200,\"sys\":{\"type\":1,\"id\":7289,\"message\":0.0023,\"country\":\"RU\",\"sunrise\":1504224820,\"sunset\":1504274249},\"id\":1496153,\"name\":\"Omsk\",\"cod\":200}"

    @Mock
    lateinit var type: Type

    @Mock
    lateinit var jsonElement: JsonElement

    @Mock
    lateinit var jdc: JsonDeserializationContext

    lateinit var jsonObject: JsonObject

    lateinit var deserializer: WeatherDeserializer

    lateinit var weatherEntity: WeatherEntity

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        jsonObject = JsonParser().parse(weatherApiString).asJsonObject
        deserializer = WeatherDeserializer()
        weatherEntity = WeatherEntity("Omsk", 1504224820, 1504274249, 23, "01n")
    }

    @Test
    fun transform_Test() {
        `when`(jsonElement.asJsonObject).thenReturn(jsonObject)

        val entity = deserializer.deserialize(jsonElement, type, jdc)
        assertEquals(entity, weatherEntity)
    }
}