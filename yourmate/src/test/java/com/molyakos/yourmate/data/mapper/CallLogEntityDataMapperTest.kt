package com.molyakos.yourmate.data.mapper

import com.molyakos.yourmate.R
import com.molyakos.yourmate.app.IResourceManager
import com.molyakos.yourmate.data.entity.CallLogEntity
import com.molyakos.yourmate.data.entity.mapper.CallLogEntityDataMapper
import com.molyakos.yourmate.domain.models.CallLogModel
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

@RunWith(MockitoJUnitRunner::class)
class CallLogEntityDataMapperTest {

    @Mock
    lateinit var resourceManager: IResourceManager

    lateinit var callLogEntityList: List<CallLogEntity>
    lateinit var callLogModelList: List<CallLogModel>

    lateinit var mapper: CallLogEntityDataMapper

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        mapper = CallLogEntityDataMapper(resourceManager)

        val callLogEntity = CallLogEntity("name", "8800", 1, "1", "1")
        callLogEntityList = listOf(callLogEntity)

        val callLogModel = CallLogModel("name")
        callLogModel.number = "8800"
        callLogModel.type = "1"
        callLogModel.date = "1"
        callLogModel.duration = "1"
        callLogModelList = listOf(callLogModel)
    }

    @Test
    fun transform_Test() {
        `when`(resourceManager.getString(R.string.number_unknown)).thenReturn("number_unknown")
        `when`(resourceManager.getString(R.string.missed_call)).thenReturn("missed_call")
        `when`(resourceManager.getString(R.string.outgoing_call)).thenReturn("outgoing_call")
        `when`(resourceManager.getString(R.string.incoming_call)).thenReturn("incoming_call")

        val resultList = mapper.transform(callLogEntityList)
        assertEquals(resultList, callLogModelList)
    }
}