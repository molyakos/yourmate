package com.molyakos.yourmate.data.mapper

import com.molyakos.yourmate.app.IDateFormatter
import com.molyakos.yourmate.data.entity.WeatherEntity
import com.molyakos.yourmate.data.entity.mapper.WeatherEntityDataMapper
import com.molyakos.yourmate.domain.models.WeatherModel
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

@RunWith(MockitoJUnitRunner::class)
class WeatherEntityDataMapperTest {

    @Mock
    lateinit var dateFormat: IDateFormatter

    lateinit var mapper: WeatherEntityDataMapper

    lateinit var weatherEntity: WeatherEntity
    lateinit var weatherModel: WeatherModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        mapper = WeatherEntityDataMapper(dateFormat)
        weatherEntity = WeatherEntity("Moscow", 0, 1, 20, "01n")
        weatherModel = WeatherModel("Moscow", "0", "1", "20", "01n")
    }

    @Test
    fun transform_Test() {
        val format = "HH:mm"
        val sunrise = weatherEntity.sunrise.toString()
        val sunset = weatherEntity.sunset.toString()
        `when`(dateFormat.toStringDate(weatherEntity.sunrise, format)).thenReturn(sunrise)
        `when`(dateFormat.toStringDate(weatherEntity.sunset, format)).thenReturn(sunset)

        val model = mapper.transform(weatherEntity)
        assertEquals(model, weatherModel)
    }
}