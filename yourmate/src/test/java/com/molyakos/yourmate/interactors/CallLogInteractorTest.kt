package com.molyakos.yourmate.interactors

import com.molyakos.yourmate.domain.interactors.CallLogInteractor
import com.molyakos.yourmate.domain.interactors.ICallLogInteractor
import com.molyakos.yourmate.domain.models.CallLogModel
import com.molyakos.yourmate.domain.models.WeatherModel
import com.molyakos.yourmate.domain.repository.ICallLogRepository
import com.molyakos.yourmate.utils.IRxSchedulers
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.junit.Assert.*
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CallLogInteractorTest {

    @Mock
    lateinit var callLogRepository: ICallLogRepository

    @Mock
    lateinit var schedulers: IRxSchedulers

    lateinit var interactor: ICallLogInteractor

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        interactor = CallLogInteractor(callLogRepository, schedulers)
        `when`<ObservableTransformer<Observable<WeatherModel>, Observable<WeatherModel>>>(schedulers.applySchedulers())
                .thenReturn(ObservableTransformer { upstream ->
                    upstream.subscribeOn(Schedulers.io())
                })
    }

    @Test
    fun updateCallLog_Test() {
        val list = emptyList<CallLogModel>()
        val listObservable = Observable.just(list)
        `when`(callLogRepository.fetchCallLogList()).thenReturn(listObservable)

        interactor.updateCallLog()
        assertEquals(callLogRepository.fetchCallLogList(), listObservable)
    }

    @Test
    fun updateMissedCallCount_Test() {
        val missedCallCountObservable = Observable.just(1)
        `when`(callLogRepository.fetchMissedCallsCount()).thenReturn(missedCallCountObservable)

        interactor.updateMissedCallCount()
        assertEquals(callLogRepository.fetchMissedCallsCount(), missedCallCountObservable)
    }
}