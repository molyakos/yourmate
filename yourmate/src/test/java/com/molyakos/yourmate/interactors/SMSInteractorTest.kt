package com.molyakos.yourmate.interactors

import com.molyakos.yourmate.domain.interactors.ISMSInteractor
import com.molyakos.yourmate.domain.interactors.SMSInteractor
import com.molyakos.yourmate.domain.models.WeatherModel
import com.molyakos.yourmate.domain.repository.ISMSRepository
import com.molyakos.yourmate.utils.IRxSchedulers
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.junit.Assert.*
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SMSInteractorTest {

    @Mock
    lateinit var smsRepository: ISMSRepository

    @Mock
    lateinit var schedulers: IRxSchedulers

    lateinit var interactor: ISMSInteractor

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        interactor = SMSInteractor(smsRepository, schedulers)
        `when`<ObservableTransformer<Observable<WeatherModel>, Observable<WeatherModel>>>(schedulers.applySchedulers())
                .thenReturn(ObservableTransformer { upstream ->
                    upstream.subscribeOn(Schedulers.io())
                })
    }

    @Test
    fun fetchMissedSMSCount_Test() {
        val missedSMSCountObservable = Observable.just(1)
        `when`(smsRepository.fetchMissedSMSCount()).thenReturn(missedSMSCountObservable)

        interactor.updateMissedSMSCount()
        assertEquals(smsRepository.fetchMissedSMSCount(), missedSMSCountObservable)
    }
}