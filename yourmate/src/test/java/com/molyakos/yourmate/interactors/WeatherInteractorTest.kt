package com.molyakos.yourmate.interactors

import org.junit.Assert.*
import com.molyakos.yourmate.domain.interactors.IWeatherInteractor
import com.molyakos.yourmate.domain.interactors.WeatherInteractor
import com.molyakos.yourmate.domain.models.WeatherModel
import com.molyakos.yourmate.domain.repository.IWeatherRepository
import com.molyakos.yourmate.utils.IRxSchedulers
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class WeatherInteractorTest {

    @Mock
    lateinit var weatherRepository: IWeatherRepository

    @Mock
    lateinit var schedulers: IRxSchedulers

    lateinit var interactor: IWeatherInteractor

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        interactor = WeatherInteractor(weatherRepository, schedulers)
        `when`<ObservableTransformer<Observable<WeatherModel>, Observable<WeatherModel>>>(schedulers.applySchedulers())
                .thenReturn(ObservableTransformer { upstream ->
                    upstream.subscribeOn(Schedulers.io())
                })
    }

    @Test
    fun updateWeather_Test() {
        val cityName = ""
        val tempUnits = ""
        val weatherModel = WeatherModel("", "", "", "", "")
        val weatherObservable = Observable.just(weatherModel)
        `when`(weatherRepository.fetchWeatherFromCloudStore(cityName, tempUnits)).thenReturn(weatherObservable)
        `when`(weatherRepository.fetchWeatherFromDiskStore()).thenReturn(weatherObservable)

        interactor.updateWeather(cityName, tempUnits)

        assertEquals(weatherRepository.fetchWeatherFromCloudStore(cityName, tempUnits), weatherObservable)
        assertEquals(weatherRepository.fetchWeatherFromDiskStore(), weatherObservable)
    }
}