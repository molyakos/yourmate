package com.molyakos.yourmate.repositories

import com.google.gson.Gson
import com.molyakos.yourmate.data.entity.WeatherEntity
import com.molyakos.yourmate.data.repository.weather.WeatherRepository
import com.molyakos.yourmate.data.repository.weather.datastore.IWeatherCloudStore
import com.molyakos.yourmate.data.entity.mapper.WeatherEntityDataMapper
import com.molyakos.yourmate.data.repository.weather.datastore.IWeatherDiskStore
import com.molyakos.yourmate.domain.repository.IWeatherRepository
import io.reactivex.Observable
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class WeatherRepositoryTest {

    val weatherApiString = "{\"coord\":{\"lon\":73.4,\"lat\":55},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"base\":\"stations\",\"main\":{\"temp\":23,\"pressure\":1012,\"humidity\":49,\"temp_min\":23,\"temp_max\":23},\"visibility\":10000,\"wind\":{\"speed\":3,\"deg\":330},\"clouds\":{\"all\":0},\"dt\":1504276200,\"sys\":{\"type\":1,\"id\":7289,\"message\":0.0023,\"country\":\"RU\",\"sunrise\":1504224820,\"sunset\":1504274249},\"id\":1496153,\"name\":\"Omsk\",\"cod\":200}"
    val weatherEntity = WeatherEntity("Omsk", 1504224820, 1504274249, 20, "01n")

    @Mock
    lateinit var mapper: WeatherEntityDataMapper

    @Mock
    lateinit var diskStore: IWeatherDiskStore

    @Mock
    lateinit var cloudStore: IWeatherCloudStore

    lateinit var gson: Gson

    lateinit var repository: IWeatherRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        gson = Gson()
        repository = WeatherRepository(mapper, diskStore, cloudStore, gson)
    }

    @Test
    fun fetchWeatherFromCloudStore_Test() {
        val cityName = ""
        val tempUnits = ""
        val cloudJsonObservable = Observable.just(weatherApiString)
        val diskJsonObservable = Observable.just(weatherApiString)
        `when`(diskStore.fetchWeather()).thenReturn(diskJsonObservable)
        `when`(cloudStore.fetchWeather(cityName, tempUnits)).then {
            repository.writeWeatherToDiskStore(weatherEntity)
            return@then cloudJsonObservable
        }

        repository.fetchWeatherFromCloudStore(cityName, tempUnits)
        assertEquals(diskStore.fetchWeather(), diskJsonObservable)
        assertEquals(cloudStore.fetchWeather(cityName, tempUnits), cloudJsonObservable)
    }

    @Test
    fun getWeatherFromDiskStore_Test() {
        val jsonObservable = Observable.just("")
        `when`(diskStore.fetchWeather()).thenReturn(jsonObservable)

        repository.fetchWeatherFromDiskStore()
        assertEquals(diskStore.fetchWeather(), jsonObservable)
    }

    @Test
    fun writeWeatherToDiskStore_Test() {
        // как тестировать этот метод?
    }
}