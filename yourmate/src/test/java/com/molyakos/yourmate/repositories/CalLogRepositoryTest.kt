package com.molyakos.yourmate.repositories

import org.junit.Assert.*
import com.molyakos.yourmate.data.entity.CallLogEntity
import com.molyakos.yourmate.data.entity.mapper.CallLogEntityDataMapper
import com.molyakos.yourmate.data.repository.calllog.CallLogRepository
import com.molyakos.yourmate.data.repository.calllog.datastore.ICallLogDiskStore
import com.molyakos.yourmate.domain.repository.ICallLogRepository
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CalLogRepositoryTest {

    @Mock
    lateinit var diskStore: ICallLogDiskStore

    @Mock
    lateinit var mapper: CallLogEntityDataMapper

    lateinit var repository: ICallLogRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        repository = CallLogRepository(mapper, diskStore)
    }

    @Test
    fun fetchMissedCallsCount_Test() {
        val observableMissedCalls = Observable.just(1)
        `when`(diskStore.fetchMissedCallsCount()).thenReturn(observableMissedCalls)

        repository.fetchMissedCallsCount()
        assertEquals(diskStore.fetchMissedCallsCount(), observableMissedCalls)
    }

    @Test
    fun fetchCallLogList_Test() {
        val observableList = Observable.just(emptyList<CallLogEntity>())
        `when`(diskStore.fetchCallLogList()).thenReturn(observableList)

        repository.fetchCallLogList()
        assertEquals(diskStore.fetchCallLogList(), observableList)
    }
}