package com.molyakos.yourmate.repositories

import com.molyakos.yourmate.data.repository.sms.SMSRepository
import com.molyakos.yourmate.data.repository.sms.datastore.ISMSDiskStore
import com.molyakos.yourmate.domain.repository.ISMSRepository
import io.reactivex.Observable
import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Mockito.*
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations

@RunWith(MockitoJUnitRunner::class)
class SMSRepositoryTest {

    @Mock
    lateinit var diskStore: ISMSDiskStore

    lateinit var repository: ISMSRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        repository = SMSRepository(diskStore)
    }

    @Test
    fun fetchMissedSMSCount_Test() {
        val missedSMSCountObservable = Observable.just(1)
        `when`(diskStore.fetchMissedSMSCount()).thenReturn(missedSMSCountObservable)

        repository.fetchMissedSMSCount()

        assertEquals(diskStore.fetchMissedSMSCount(), missedSMSCountObservable)
    }
}