package com.molyakos.yourmate.presenters

import com.molyakos.yourmate.domain.controllers.IToneController
import com.molyakos.yourmate.domain.interactors.ICallLogInteractor
import com.molyakos.yourmate.domain.models.CallLogModel
import com.molyakos.yourmate.presentation.presenter.module.phone.IPhonePresenter
import com.molyakos.yourmate.presentation.presenter.module.phone.PhonePresenter
import com.molyakos.yourmate.presentation.view.module.phone.IPhoneView
import io.reactivex.Observable
import org.junit.After
import org.junit.Before
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner::class)
class PhonePresenterTest {

    @Mock
    lateinit var view: IPhoneView

    @Mock
    lateinit var callLogInteractor: ICallLogInteractor

    @Mock
    lateinit var toneController: IToneController

    lateinit var presenter: IPhonePresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        presenter = PhonePresenter(toneController, callLogInteractor)
        presenter.bind(view)
    }

    @Test
    fun updateMissedCallsCount_Test() {
        val emptyList = emptyList<CallLogModel>()
        val listObservable = Observable.just(emptyList)
        `when`(callLogInteractor.updateCallLog()).thenReturn(listObservable)

        presenter.updateCallLog()
        assertEquals(callLogInteractor.updateCallLog(), listObservable)
        verify(view).updateCallLogList(emptyList)
    }

    @After
    fun destroy() {
        presenter.unbind()
    }
}