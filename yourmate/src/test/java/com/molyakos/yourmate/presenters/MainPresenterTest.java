package com.molyakos.yourmate.presenters;

import com.molyakos.yourmate.presentation.presenter.module.main.IMainPresenter;
import com.molyakos.yourmate.presentation.presenter.module.main.MainPresenter;
import com.molyakos.yourmate.presentation.view.module.main.IMainView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {

    @Mock
    private IMainView view;

    private IMainPresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new MainPresenter();
    }

    @Test
    public void BindUnbind_Test() {
        presenter.bind(view);
        presenter.unbind();
    }
}
