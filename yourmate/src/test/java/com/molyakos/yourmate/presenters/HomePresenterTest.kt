package com.molyakos.yourmate.presenters

import com.molyakos.yourmate.domain.interactors.ICallLogInteractor
import com.molyakos.yourmate.domain.interactors.ISMSInteractor
import com.molyakos.yourmate.domain.interactors.ISettingsInteractor
import com.molyakos.yourmate.domain.interactors.IWeatherInteractor
import com.molyakos.yourmate.domain.models.WeatherModel
import org.junit.Assert.*
import com.molyakos.yourmate.presentation.presenter.module.home.HomePresenter
import com.molyakos.yourmate.presentation.presenter.module.home.IHomePresenter
import com.molyakos.yourmate.presentation.view.module.home.IHomeView
import io.reactivex.Observable
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HomePresenterTest {

    @Mock
    lateinit var view: IHomeView

    @Mock
    lateinit var smsInteractor: ISMSInteractor

    @Mock
    lateinit var callLogInteractor: ICallLogInteractor

    @Mock
    lateinit var weatherInteractor: IWeatherInteractor

    @Mock
    lateinit var settingsInteractor: ISettingsInteractor

    lateinit var presenter: IHomePresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        presenter = HomePresenter(smsInteractor, callLogInteractor, weatherInteractor, settingsInteractor)
        presenter.bind(view)
    }

    @Test
    fun updateMissedCallsCount_Test() {
        val missedCallCount = 1
        val missedCallCountObservable = Observable.just(missedCallCount)
        `when`(callLogInteractor.updateMissedCallCount()).thenReturn(missedCallCountObservable)

        presenter.updateMissedCallsCount()

        assertEquals(callLogInteractor.updateMissedCallCount(), missedCallCountObservable)
        verify(view).updateMissedCallsCount(missedCallCount)
    }

    @Test
    fun updateMissedSMSCount_Test() {
        val missedSMSCount = 1
        val missedSMSCountObservable = Observable.just(missedSMSCount)
        `when`(smsInteractor.updateMissedSMSCount()).thenReturn(missedSMSCountObservable)

        presenter.updateMissedSMSCount()
        assertEquals(smsInteractor.updateMissedSMSCount(), missedSMSCountObservable)
        verify(view).updateMissedSMSCount(missedSMSCount)
    }

    @Test
    fun updateWeatherEmptyError_Test() {
        val cityName = ""
        val tempUnits = ""
        val expectedException = IllegalStateException("test")
        val weatherModelObservable = Observable.error<WeatherModel>(expectedException)
        `when`(weatherInteractor.updateWeather(cityName, tempUnits)).thenReturn(weatherModelObservable)
        `when`(settingsInteractor.fetchCurrentCityName()).thenReturn(cityName)
        `when`(settingsInteractor.fetchUnits()).thenReturn(tempUnits)

        presenter.updateWeather()
        assertEquals(weatherInteractor.updateWeather(cityName, tempUnits), weatherModelObservable)
        verify(view).errorUpdateWeather()
    }

    @Test
    fun updateWeatherEmptySuccess_Test() {
        val cityName = ""
        val tempUnits = ""
        val weatherModel = WeatherModel("Moscow", "6:40", "21:42", "10", "01n")
        val weatherModelObservable = Observable.just(weatherModel)
        `when`(weatherInteractor.updateWeather(cityName, tempUnits)).thenReturn(weatherModelObservable)
        `when`(settingsInteractor.fetchCurrentCityName()).thenReturn(cityName)
        `when`(settingsInteractor.fetchUnits()).thenReturn(tempUnits)

        presenter.updateWeather()
        assertEquals(weatherInteractor.updateWeather(cityName, tempUnits), weatherModelObservable)
        verify(view).updateWeather(weatherModel)
    }

    @Test
    fun updateWeatherWithArgumentsError_Test() {
        val cityName = ""
        val tempUnits = ""
        val expectedException = IllegalStateException("test")
        val weatherModelObservable = Observable.error<WeatherModel>(expectedException)
        `when`(weatherInteractor.updateWeather(cityName, tempUnits)).thenReturn(weatherModelObservable)

        presenter.updateWeather(cityName, tempUnits)
        assertEquals(weatherInteractor.updateWeather(cityName, tempUnits), weatherModelObservable)
        verify(view).errorUpdateWeather()
    }

    @Test
    fun updateWeatherWithArgumentsSuccess_Test() {
        val cityName = ""
        val tempUnits = ""
        val weatherModel = WeatherModel("Moscow", "6:40", "21:42", "10", "01n")
        val weatherModelObservable = Observable.just(weatherModel)
        `when`(weatherInteractor.updateWeather(cityName, tempUnits)).thenReturn(weatherModelObservable)

        presenter.updateWeather(cityName, tempUnits)
        assertEquals(weatherInteractor.updateWeather(cityName, tempUnits), weatherModelObservable)
        verify(view).updateWeather(weatherModel)
    }

    @After
    fun destroy() {
        presenter.unbind()
    }
}
